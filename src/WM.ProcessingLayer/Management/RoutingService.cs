﻿using Services.Contracts;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WM.Common.Entities;
using WM.DataAccess;
using WM.ProcessingLayer.Contracts;

namespace WM.ProcessingLayer.Management
{
	public class RoutingService : IRoutingService
	{
		#region Singleton
		private static object lockObj = new object();

		private static IRoutingService service;
		public static IRoutingService Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new RoutingService();

					return service;
				}
			}
		}

		#endregion

		public IList<Route> GetAllRoutes(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all routes. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Route>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all Routes. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public Location GetRoutesFor(string tuName, string target)
		{
			try
			{
				($"Get routes for [{tuName}]").LogInfo();

				var tu = TransportUnitManagement.Manager.GetByName(tuName);

				var locTo = TopologyService.Manager.GetByName(target);

				var da = DataAccessRegister.GetDataAccess<Route>();
				var routes = da.GetAllElements(nameof(Route.Enabled), true);

				var locFromParent = tu.Location;
				while (locFromParent.Parent != null && locFromParent.Type != LocationType.Warehouse)
					locFromParent = TopologyService.Manager.GetByName(locFromParent.Parent);

				var locToParent = locTo;
				while (locToParent.Parent != null && locToParent.Type != LocationType.Warehouse)
					locToParent = TopologyService.Manager.GetByName(locToParent.Parent);

				if (locFromParent.Name == locToParent.Name)
					return locTo;

				return routes.FirstOrDefault(x => x.Source.Name == locFromParent.Name && x.Target.Name == locToParent.Name)?.Next;
			}
			catch (Exception e)
			{
				($"Error getting routing by for [{tuName}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteRoute(long id)
		{
			try
			{
				($"Delete route Id [{id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Route>();
				da.DeleteElement(id);
			}
			catch (Exception e)
			{
				($"Error deleting route Id [{id}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void InsertRoute(Route route)
		{
			try
			{
				($"Insert new Route from [{route.Source?.Name}], next [{route.Next?.Name}], target [{route.Target?.Name}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Route>();
				var loc = da.GetElementById(route.Id);

				if (loc != null) throw new Exception("Route already exists");

				da.InsertElement(route);
			}
			catch (Exception e)
			{
				($"Error inserting new route.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdateRoute(Route route)
		{
			try
			{
				($"update route id [{route.Id}], from [{route.Source?.Name}], next [{route.Next?.Name}], target [{route.Target?.Name}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Route>();

				da.UpdateElement(route);
			}
			catch (Exception e)
			{
				($"Error updating route.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}
	}
}
