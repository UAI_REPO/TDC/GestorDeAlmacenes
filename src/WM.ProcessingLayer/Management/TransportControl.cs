﻿using Service.Security;
using Service.Security.Contracts;
using Services.Contracts;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WM.Common.Entities;
using WM.DataAccess;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Exceptions;

namespace WM.ProcessingLayer.Management
{
	public class TransportControl : ITransportControl
	{
		#region Singleton
		private static object lockObj = new object();

		private static ITransportControl service;
		public static ITransportControl Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new TransportControl();

					return service;
				}
			}
		}
		#endregion

		public ISecurityService SessionService => SecurityService.Manager;

		public IList<TransportOrder> GetAllTords(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all transport orders. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TransportOrder>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all transport orders. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}


		public void CreateTransportOrder(string tuname, string target)
		{
			try
			{
				var tu = TransportUnitManagement.Manager.GetAllTus(nameof(TransportUnit.Name), tuname).FirstOrDefault();
				var location = TopologyService.Manager.GetByName(target);;

				CreateTransportOrder(tu, location);
			}
			catch (Exception e)
			{
				($"Error creating transport order. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}


		public void CreateTransportOrder(TransportUnit tu, Location locTo)
		{
			try
			{
				($"Creating transport order for [{tu.Name}] target [{(locTo != null ? locTo.Name : string.Empty)}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TransportOrder>();

				var currentTord = da.GetAllElements(nameof(TransportUnit.Name), tu.Name, typeof(TransportUnit)).FirstOrDefault();
				if (currentTord != null)
					throw new TordAlreadyExistException(tu.Name);

				if (locTo == null || locTo.Capacity == 0)
					locTo = RulesManagement.Manager.GetSuitableLocationFor(tu, locTo);

				var tord = new TransportOrder();
				tord.Status = TordStatus.Created;
				tord.TargetLocation = locTo;
				tord.Tu = tu;
				tord.CreatedAt = DateTime.Now;
				tord.CreatedBy = SessionService.GetCurrentUser();
				
				da.InsertElement(tord);

				ChangeTordStatus(tord, tu.Location);
			}
			catch (Exception e)
			{
				($"Error creating transport order. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void ChangeTordStatus(TransportOrder tord, Location newLoc)
		{
			var da = DataAccessRegister.GetDataAccess<TransportOrder>();

			try
			{
				tord.LastLocation = tord.ActualLocation;
				tord.ActualLocation = newLoc;

				if (tord.TargetLocation.Name == newLoc.Name)
				{
					tord.Status = TordStatus.Completed;
					tord.UpdatedAt = DateTime.Now;
					tord.UpdatedBy = SessionService.GetCurrentUser();
					tord.Message = $"The trasnport unit has arrived";
					da.UpdateElement(tord);

					DeleteTord(tord.Id);

					if (newLoc.Class == "Delivery")
					{	
						TransportUnitManagement.Manager.DeleteTU(tord.Tu.Name);
					}
					return;
				}
				
				var loc = RoutingService.Manager.GetRoutesFor(tord.Tu.Name, tord.TargetLocation.Name);

				if (loc == null)
				{ 
					tord.Status = TordStatus.Faulty;
					tord.Message = $"No way to target";
					tord.UpdatedAt = DateTime.Now;
					tord.UpdatedBy = SessionService.GetCurrentUser();
					da.UpdateElement(tord);
					return;
				}

				if (loc.Capacity == 0)
					loc = RulesManagement.Manager.GetSuitableLocationFor(tord.Tu, loc);

				tord.Status = TordStatus.Ready;
				tord.Message = $"Waiting for movement";
				tord.UpdatedAt = DateTime.Now;
				tord.UpdatedBy = SessionService.GetCurrentUser();
				da.UpdateElement(tord);
				TransportMovementService.Manager.CreateMovement(tord, loc);

			}
			catch (Exception e)
			{
				tord.Status = TordStatus.Faulty;
				tord.Message = $"Error processing Tord";
				tord.UpdatedAt = DateTime.Now;
				tord.UpdatedBy = SessionService.GetCurrentUser();
				da.UpdateElement(tord);
				($"Error creating transport order. ").LogError(e);
				return;
			}
		}

		public void DeleteTord(long id)
		{
			try
			{
				($"Deleting transport order. Id [{id}]").LogInfo();

				var dat = DataAccessRegister.GetDataAccess<TordMovement>();

				var movs = dat.GetAllElements(nameof(TransportOrder.Id), id, typeof(TransportOrder));

				foreach (var mov in movs)
				{
					dat.DeleteElement(mov.Id);
				}

				var da = DataAccessRegister.GetDataAccess<TransportOrder>();
				da.DeleteElement(id);
			}
			catch (Exception e)
			{
				($"Error deleting transport order. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public IList<TransportOrderHist> GetAllTordsHist(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting historical transport orders. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TransportOrderHist>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting historical transport orders. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

	}
}
