﻿using Services.Contracts;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using WM.Common.Entities;
using WM.DataAccess;
using WM.ProcessingLayer.Contracts;

namespace WM.ProcessingLayer.Management
{
	public class MaterialService : IMaterialService
	{
		#region Singleton
		private static object lockObj = new object();

		private static IMaterialService service;
		public static IMaterialService Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new MaterialService();

					return service;
				}
			}
		}
		#endregion

		public IList<Material> GetAllMaterials(string prop = null, object value = null)
		{
			try
			{
				($"Getting all materials. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Material>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all materials. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}

		}
		public Material GetById(string id)
		{
			try
			{
				($"Get material by id [{id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Material>();
				return da.GetElementById(id);
			}
			catch (Exception e)
			{
				($"Error getting Material by id [{id}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void InsertMaterial(Material material)
		{
			try
			{
				($"Insert new Material name [{material.Name}], description [{material.Description}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Material>();
				var loc = da.GetElementById(material.Name);

				if (loc != null) throw new Exception("Material already exists");

				da.InsertElement(material);
			}
			catch (Exception e)
			{
				($"Error inserting new location.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdateMaterial(Material material)
		{
			try
			{
				($"update material name [{material.Name}], description [{material.Description}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Material>();

				da.UpdateElement(material);
			}
			catch (Exception e)
			{
				($"Error updating material.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteMaterial(string name)
		{
			try
			{
				($"Delete material Name [{name}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Material>();
				da.DeleteElement(name);
			}
			catch (Exception e)
			{
				($"Error deleting material Name [{name}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}
	}
}
