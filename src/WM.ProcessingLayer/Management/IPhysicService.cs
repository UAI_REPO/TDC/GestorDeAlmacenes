﻿using System;

namespace ProcessingDataLayer
{
	public interface IPhysicService
	{
		public float CalculateVoltage(float resistance, float current);
		public float CalculateCurrent(float resistance, float voltage);
		public float CalculateResistance(float current, float voltage);
		Tuple<double, double> CalculateParabolicShot(float speed, float angle);
	}
}