﻿using Newtonsoft.Json;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using Service.Security;
using Services.Contracts;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WM.Common.Entities;
using WM.DataAccess;

namespace WM.ProcessingLayer.Management
{
	public class TransportMovementService : ITransportMovementService
	{
		#region Singleton
		private static object lockObj = new object();

		private static ITransportMovementService service;
		public static ITransportMovementService Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new TransportMovementService();

					return service;
				}
			}
		}
		#endregion

		public IList<TordMovement> GetAllMovements(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all movements. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TordMovement>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all movements. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void CreateMovement(TransportOrder tord, Location loc)
		{
			try
			{
				($"Creating transport movement. Tu [{tord.Tu.Name}] Location [{loc.Name}] ").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TordMovement>();

				var mov = new TordMovement();
				mov.Status = TordMovementStatus.Active;
				mov.Tord = tord;
				mov.TargetLocation = loc;
				mov.Tu = tord.Tu;
				
				da.InsertElement(mov);
			}
			catch (Exception e)
			{
				($"Error creating movement.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void Pickup(string tu)
		{
			try
			{
				($"Picking up transport unit. Tu [{tu}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TordMovement>();

				var movements = da.GetAllElements(nameof(TransportUnit.Name), tu, typeof(TransportUnit));
				var mov = movements.FirstOrDefault(x => x.Status != TordMovementStatus.Completed);

				if (mov.Status == TordMovementStatus.Paused)
				{
					($"Current movement is in pause.").LogInfo();
					return;
				}

				if (mov.Status == TordMovementStatus.Pickup)
				{
					($"Transport unit already picked up.").LogInfo();
					return;
				}

				mov.Status = TordMovementStatus.Pickup;

				da.UpdateElement(mov);
			}
			catch (Exception e)
			{
				($"Error picking up.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void Deposit(string tu, string location)
		{
			try
			{
				($"Deposit transport unit. Tu [{tu}] Location [{location}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TordMovement>();

				var loc = TopologyService.Manager.GetByName(location);
				var movements = da.GetAllElements(nameof(TransportUnit.Name), tu, typeof(TransportUnit));
				var mov = movements.FirstOrDefault(x => x.Status != TordMovementStatus.Completed);

				if (mov.Status == TordMovementStatus.Paused)
				{
					($"Current movement is in pause.").LogInfo();
					return;
				}

				if (mov.Status == TordMovementStatus.Deposit)
				{
					($"Transport unit already deposited.").LogInfo();
					return;
				}

				mov.Status = TordMovementStatus.Deposit;

				da.UpdateElement(mov);

				TransportUnitManagement.Manager.MoveTu(mov.Tu, loc);

				mov.Status = TordMovementStatus.Completed;
				da.UpdateElement(mov);

				da.DeleteElement(mov.Id);

				TransportControl.Manager.ChangeTordStatus(mov.Tord, loc);
			}
			catch (Exception e)
			{
				($"Error picking up.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public IList<TordMovementHist> GetAllMovementsHist(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting historical movements. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TordMovementHist>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting historical movements. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void GenerateMovementsReport(string path)
		{
			try
			{
				PdfDocument document = new PdfDocument();
				PdfPage page = document.AddPage();
				XGraphics gfx = XGraphics.FromPdfPage(page);
				XFont titleFont = new XFont("Arial", 25, XFontStyle.Bold);
				XFont subtitleFont = new XFont("Arial", 15, XFontStyle.Regular);
				XFont tableHeaderFont = new XFont("Arial", 12, XFontStyle.Bold);
				XFont tableDataFont = new XFont("Arial", 12, XFontStyle.Regular);

				XImage image = XImage.FromFile("./Content/Images/report_header.png");
				gfx.DrawImage(image, 0, 0);

				//title
				double xpos = 0;
				double ypos = 30;
				double boxsize_x = page.Width;
				double boxsize_y = 80;
				gfx.DrawString("report_title".Translate(), titleFont, XBrushes.Black,
				new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);

				//User Date row
				xpos = 30;
				ypos += boxsize_y;
				boxsize_y = 40;
				boxsize_x = 70;
				gfx.DrawString("report_user".Translate() + SecurityService.Manager.GetCurrentUser(), subtitleFont, XBrushes.Black,
				new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.CenterLeft);

				xpos = page.Width.Value - 100;
				boxsize_y = 40;
				boxsize_x = 70;
				gfx.DrawString("report_date".Translate() + DateTime.Now, subtitleFont, XBrushes.Black,
				new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.CenterRight);

				//Table
				xpos = 30;
				ypos += boxsize_y + 70;
				boxsize_y = 30;
				boxsize_x = 85;

				gfx.DrawString("report_table_id".Translate(), tableHeaderFont, XBrushes.DarkGray,
				new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);
				xpos += boxsize_x;

				gfx.DrawString("report_table_tu".Translate(), tableHeaderFont, XBrushes.DarkGray,
				new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);
				xpos += boxsize_x;

				gfx.DrawString("report_table_from".Translate(), tableHeaderFont, XBrushes.DarkGray,
				new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);
				xpos += boxsize_x;
				
				gfx.DrawString("report_table_status".Translate(), tableHeaderFont, XBrushes.DarkGray,
				new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);
				xpos += boxsize_x;

				gfx.DrawString("report_table_tord".Translate(), tableHeaderFont, XBrushes.DarkGray,
				new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);
				xpos += boxsize_x;

				gfx.DrawString("report_table_target".Translate(), tableHeaderFont, XBrushes.DarkGray,
				new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);

				xpos = 30;
				ypos += boxsize_y;
				boxsize_x = page.Width - 30;

				var pen = new XPen(XColors.DarkGray, 1);
				pen.DashStyle = XDashStyle.Solid;
				gfx.DrawLine(pen, xpos, ypos, boxsize_x, ypos);

				var movs = GetAllMovements();

				foreach (var mov in movs)
				{
					xpos = 30;
					ypos += boxsize_y;
					boxsize_y = 30;
					boxsize_x = 85;

					gfx.DrawString(mov.Id.ToString(), tableDataFont, XBrushes.DarkGray,
					new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);
					xpos += boxsize_x;

					gfx.DrawString(mov.Tu.Name, tableDataFont, XBrushes.DarkGray,
					new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);
					xpos += boxsize_x;

					gfx.DrawString(mov.Tu.Location.Name, tableDataFont, XBrushes.DarkGray,
					new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);
					xpos += boxsize_x;

					gfx.DrawString(mov.Status.ToString(), tableDataFont, XBrushes.DarkGray,
					new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);
					xpos += boxsize_x;

					gfx.DrawString(mov.Tord.Id.ToString(), tableDataFont, XBrushes.DarkGray,
					new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);
					xpos += boxsize_x;

					gfx.DrawString(mov.TargetLocation.Name, tableDataFont, XBrushes.DarkGray,
					new XRect(xpos, ypos, boxsize_x, boxsize_y), XStringFormats.Center);

				}


				document.Save(path);

			}
			catch (Exception)
			{

				throw;
			}
		}

		public void SerializeMovements(string path)
		{
			var movs = GetAllMovements();

			var data = JsonConvert.SerializeObject(movs);

			File.WriteAllText(path, data);
		}
	}
}
