﻿using Services.Contracts;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WM.Common.Entities;
using WM.DataAccess;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Exceptions;
using WM.ProcessingLayer.Management.SearchRules;

namespace WM.ProcessingLayer.Management
{
	public class RulesManagement : IRulesManagement
	{
		#region Singleton
		private static object lockObj = new object();

		private static IRulesManagement service;
		public static IRulesManagement Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new RulesManagement();

					return service;
				}
			}
		}

		#endregion
		public IList<Rule> GetAllRules(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all rules. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Rule>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all Rules. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public Location GetSuitableLocationFor(TransportUnit tu, Location parent = null)
		{
			try
			{
				($"Get Location For TU [{tu.Name}]").LogInfo();

				if (parent?.Capacity > 0)
					return parent;

				return CallStrategiesFor(tu, parent);
			}
			catch (Exception e)
			{
				($"Error getting location by strategies.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		private Location CallStrategiesFor(TransportUnit tu, Location parent)
		{
			var da = DataAccessRegister.GetDataAccess<Rule>();
			var rules = da.GetAllElements(nameof(Rule.Enabled), true).Where(x => x.Type.Name == tu.Material.Name).OrderBy(x => x.Priority);

			var currentLevel = parent != null ? (LocationType)((int)parent.Type + 1): LocationType.Warehouse;

			IList<Location> result = new List<Location>();

			if (parent != null)
				result.Add(parent);

			if (currentLevel == LocationType.Warehouse)
			{
				result = GetLocationsForLevel(tu, rules.Where(x => x.Level == currentLevel), currentLevel, result);
				currentLevel = LocationType.Bay;
			}

			if (currentLevel == LocationType.Bay)
			{
				result = GetLocationsForLevel(tu, rules.Where(x => x.Level == currentLevel), currentLevel, result);
				currentLevel = LocationType.Area;
			}

			if (currentLevel == LocationType.Area)
			{
				result = GetLocationsForLevel(tu, rules.Where(x => x.Level == currentLevel), currentLevel, result);
				currentLevel = LocationType.Bin;
			}

			if (currentLevel == LocationType.Bin)
			{
				result = GetLocationsForLevel(tu, rules.Where(x => x.Level == currentLevel), currentLevel, result);
				currentLevel = LocationType.Position;
			}

			if (currentLevel == LocationType.Position)
			{
				result = GetLocationsForLevel(tu, rules.Where(x => x.Level == currentLevel), currentLevel, result);
			}

			return result.FirstOrDefault();
		}

		private IList<Location> GetLocationsForLevel(TransportUnit tu, IEnumerable<Rule> rules, LocationType level, IList<Location> parents)
		{
			List<Location> result = new List<Location>();
			foreach (var rule in rules)
			{
				if (rule.Name == nameof(FreeLocationRule))
					result.AddRange(FreeLocationRule.Manager.GetLocations(tu, level, parents, rule));
				else if (rule.Name == nameof(ParameterizedRule))
					result.AddRange(ParameterizedRule.Manager.GetLocations(tu, level, parents, rule));
				else
					throw new RuleNotFoundException(rule.Name);
			}

			return result;
		}

		public void InsertRule(Rule rule)
		{
			try
			{
				($"Insert new Rule name [{rule.Name}], type [{rule.Type}], level:{rule.Level} priority:{rule.Priority} enabled:{rule.Enabled}").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Rule>();
				
				da.InsertElement(rule);
			}
			catch (Exception e)
			{
				($"Error inserting new rule.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdateRule(Rule rule)
		{
			try
			{
				($"update rule name [{rule.Name}], type [{rule.Type}], level:{rule.Level} priority:{rule.Priority} enabled:{rule.Enabled}").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Rule>();

				da.UpdateElement(rule);
			}
			catch (Exception e)
			{
				($"Error updating rule.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteRule(long id)
		{
			try
			{
				($"Delete rule Name [{id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Rule>();
				da.DeleteElement(id);
			}
			catch (Exception e)
			{
				($"Error deleting rule Name [{id}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public IList<ParameterizedDetail> GetAllParameterizedRules(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all parameterized rules. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<ParameterizedDetail>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all parameterized Rules. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void InsertParameterizedRule(ParameterizedDetail rule)
		{
			try
			{
				($"Insert new Rule Source [{rule.SourceEntity}.{rule.SourceProperty}] Value:{rule.SourceValue}, Target [{rule.TargetEntity}.{rule.TargetProperty}] Value: {rule.TargetValue}").LogInfo();

				var da = DataAccessRegister.GetDataAccess<ParameterizedDetail>();

				da.InsertElement(rule);
			}
			catch (Exception e)
			{
				($"Error inserting new rule.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdateParameterizedRule(ParameterizedDetail rule)
		{
			try
			{
				($"update parameterized rule id [{rule.Id}], source entity [{rule.SourceEntity}], property:{rule.SourceProperty} value:{rule.SourceValue}, target entity [{rule.TargetProperty}] property:{rule.TargetProperty} value:{rule.TargetValue}").LogInfo();

				var da = DataAccessRegister.GetDataAccess<ParameterizedDetail>();

				da.UpdateElement(rule);
			}
			catch (Exception e)
			{
				($"Error updating parameterized rule.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteParameterizedRule(long id)
		{
			try
			{
				($"Delete parameterized rule Name [{id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<ParameterizedDetail>();
				da.DeleteElement(id);
			}
			catch (Exception e)
			{
				($"Error deleting parameterized rule Name [{id}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

	}
}
