﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml.XPath;
using WM.Common.Entities;
using WM.DataAccess;

namespace WM.ProcessingLayer.Management.SearchRules
{
	public class FreeLocationRule : IRule
	{
		#region Singleton
		private static object lockObj = new object();

		private static IRule service;
		public static IRule Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new FreeLocationRule();

					return service;
				}
			}
		}

		/// <summary>
		/// Get all free locations based on level.
		/// This method search between all suitable locations
		/// Check if the location has a tu inside and 
		/// evaluate if there is a transport order
		/// with target location
		/// </summary>
		/// <param name="tu"></param>
		/// <param name="level"></param>
		/// <param name="parents"></param>
		/// <param name="rule"></param>
		/// <returns></returns>
		public IList<Location> GetLocations(TransportUnit tu, LocationType level, IList<Location> parents, Rule rule)
		{

			var da = DataAccessRegister.GetDataAccess<Location>();
			var locs = da.GetAllElements(nameof(Location.Type), (int)level).Where(x => x.Class == null);

			if (parents != null && parents.Count > 0)
				locs = locs.Where(x => parents.Any(p => p.Name == x.Parent)).ToList();

			var dat = DataAccessRegister.GetDataAccess<TransportUnit>();
			var tus = dat.GetAllElements();

			var datord = DataAccessRegister.GetDataAccess<TransportOrder>();
			var tords = datord.GetAllElements();

			foreach (var tord in tords)
			{
				var ctu = tus.FirstOrDefault(t => t.Name == tord.Tu.Name);
				ctu.Location = tord.TargetLocation;
			}

			var summary = new List<KeyValuePair<Location, IList<TransportUnit>>>();

			foreach (var loc in locs)
			{
				var loctus = tus.Where(t => t.Location.Name == loc.Name).ToList();
				summary.Add(new KeyValuePair<Location, IList<TransportUnit>>(loc, loctus));
			}

			summary = summary.Where(x => x.Key.Capacity == 0 || x.Key.Capacity > x.Value.Count).ToList();

			var result = summary.Select(x => x.Key).ToList();

			foreach (var parent in parents.Where(x => x.Capacity > 0))
			{
				result.Insert(0, parent);
			}
			
			return result;
		}
		#endregion
	}
}
