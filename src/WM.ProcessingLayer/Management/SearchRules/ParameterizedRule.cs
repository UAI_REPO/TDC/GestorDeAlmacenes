﻿using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using WM.Common.Entities;
using WM.DataAccess;

namespace WM.ProcessingLayer.Management.SearchRules
{
	public class ParameterizedRule : IRule
	{
		#region Singleton
		private static object lockObj = new object();

		private static IRule service;
		public static IRule Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new ParameterizedRule();

					return service;
				}
			}
		}

		#endregion

		public IList<Location> GetLocations(TransportUnit tu, LocationType level, IList<Location> parents, Rule rule)
		{
			$"Get location using parameters rules. Level {level}".LogInfo();

			var dar = DataAccessRegister.GetDataAccess<ParameterizedDetail>();
			var parameters = dar.GetAllElements(nameof(Rule.Id), rule.Id, typeof(Rule));

			var result = new List<Location>();
			var locs = GetAvaliableLocations(level, parents.ToList());

			foreach (var param in parameters.OrderBy(x => x.Rule.Priority))
			{
				if (!ValidateSource(tu, param))
					continue;

				$"Rule Match Source [{param.SourceEntity}] Property [{param.SourceProperty}] Value [{param.SourceValue}]. Target [{param.TargetEntity}] Property [{param.TargetProperty}] Value [{param.TargetValue}]".LogInfo();
				
				foreach (var loc in locs)
				{
					if (ValidateTarget(loc, param))
						result.Add(loc);
				}
			}

			$"Parameter Resul. Locations Count [{result.Count}] ".LogInfo();
			return result;
		}

		public bool ValidateTarget(Location loc, ParameterizedDetail param)
		{
			if (param.TargetEntity == nameof(Location))
			{
				return MatchEntityWithParameter(loc, param.TargetProperty, param.TargetValue, param.TargetOperator);
			}
			else
			{
				var prop = loc.GetType().GetProperties().FirstOrDefault(x => x.Name == param.SourceEntity);
				var objvalue = prop.GetValue(loc);

				return MatchEntityWithParameter(objvalue, param.TargetProperty, param.TargetValue, param.TargetOperator);
			}

		}
		public IList<Location> GetAvaliableLocations(LocationType level, IList<Location> parents)
		{
			var da = DataAccessRegister.GetDataAccess<Location>();
			var locs = da.GetAllElements(nameof(Location.Type), (int)level);

			if (parents != null && parents.Count > 0)
				locs = locs.Where(x => parents.Any(p => p.Name == x.Parent)).ToList();

			var dat = DataAccessRegister.GetDataAccess<TransportUnit>();
			var tus = dat.GetAllElements();

			var datord = DataAccessRegister.GetDataAccess<TransportOrder>();
			var tords = datord.GetAllElements();

			foreach (var tord in tords)
			{
				var ctu = tus.FirstOrDefault(t => t.Name == tord.Tu.Name);
				ctu.Location = tord.TargetLocation;
			}

			var summary = new List<KeyValuePair<Location, IList<TransportUnit>>>();

			foreach (var loc in locs)
			{
				var loctus = tus.Where(t => t.Location.Name == loc.Name).ToList();
				summary.Add(new KeyValuePair<Location, IList<TransportUnit>>(loc, loctus));
			}

			summary = summary.Where(x => x.Key.Capacity == 0 || x.Key.Capacity > x.Value.Count).ToList();

			return summary.Select(x => x.Key).ToList();
		}

		public bool ValidateSource(TransportUnit tu, ParameterizedDetail param)
		{
			if (param.SourceEntity == nameof(TransportUnit))
			{
				return MatchEntityWithParameter(tu, param.SourceProperty, param.SourceValue, param.SourceOperator);
			}
			else
			{
				var prop = tu.GetType().GetProperties().FirstOrDefault(x => x.Name == param.SourceEntity);
				var objvalue = prop.GetValue(tu);

				return MatchEntityWithParameter(objvalue, param.SourceProperty, param.SourceValue, param.SourceOperator);
			}
		}

		public bool MatchEntityWithParameter(object entity, string property, string value, ParameterizedOperator paramOperator)
		{
			var prop = entity.GetType().GetProperties().FirstOrDefault(x => x.Name == property);
			var objvalue = prop.GetValue(entity);

			if (objvalue == null)
				return false;

			switch (paramOperator)
			{
				case ParameterizedOperator.Equal:
					return objvalue.ToString() == value;
				case ParameterizedOperator.NotEqual:
					return objvalue.ToString() != value;
				case ParameterizedOperator.Greater:
					return Convert.ToSingle(objvalue) > Convert.ToSingle(value);
				case ParameterizedOperator.GreaterThan:
					return Convert.ToSingle(objvalue) >= Convert.ToSingle(value);
				case ParameterizedOperator.Less:
					return Convert.ToSingle(objvalue) < Convert.ToSingle(value);
				case ParameterizedOperator.LessThan:
					return Convert.ToSingle(objvalue) <= Convert.ToSingle(value);
				default:
					return false;
			}

		}
	}
}
