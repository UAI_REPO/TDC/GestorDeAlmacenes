﻿using Services.Contracts;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WM.Common.Entities;
using WM.DataAccess;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Exceptions;

namespace WM.ProcessingLayer.Management
{
	public class TransportUnitManagement : ITransportUnitManagement
	{
		#region Singleton
		private static object lockObj = new object();

		private static ITransportUnitManagement service;
		public static ITransportUnitManagement Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new TransportUnitManagement();

					return service;
				}
			}
		}
		#endregion

		public IList<TransportUnit> GetAllTus(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all transport units. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TransportUnit>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all transport units. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public TransportUnit GetByName(string name)
		{
			try
			{
				($"Get transport unit by name [{name}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TransportUnit>();
				return da.GetElementById(name);
			}
			catch (Exception e)
			{
				($"Error getting transport unit by name [{name}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public TransportUnit ReceiveTu(string lotId, string materialId, bool isObserved = false, string name = null)
		{
			try
			{
				if (string.IsNullOrEmpty(name))
					name = $"TU_{GetNextTuID():000000}";

				($"Receiving new Transport Unit name [{name}]").LogInfo();
				var tu = CreateNewTu(name, lotId, materialId, isObserved);

				var loc = RulesManagement.Manager.GetSuitableLocationFor(tu);
				//if (loc == null)
				//	NotSituableLocationException();

				TransportControl.Manager.CreateTransportOrder(tu, loc);
				
				return tu;
			}
			catch (Exception e)
			{
				($"Error inserting new transport unit.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		private int GetNextTuID()
		{
			var tus = TransportControl.Manager.GetAllTordsHist().Select(x => x.Tu).Where(x => x != null && x.Name != null).Distinct();
			int result = 0;
			foreach (var name in tus.Select(x => x.Name.Remove(0, 3)))
			{
				int number = 0;
				int.TryParse(name, out number);
				if (number > result)
					result = number;
			}

			return result + 1;
		}

		public void MoveTu(TransportUnit tu, Location location)
		{
			try
			{
				($"Moving Transport Unit name [{tu.Name}] Location [{location.Name}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TransportUnit>();

				tu.Location = location;

				da.UpdateElement(tu);
			}
			catch (Exception e)
			{
				($"Error moving transport unit.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public TransportUnit DeliveryTu(string name, string lotId)
		{
			try
			{
				($"Delivery Material Type [{name}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TransportUnit>();
				var tus = da.GetAllElements(nameof(Material.Name), name, typeof(Material));

				if (!string.IsNullOrEmpty(lotId))
					tus = tus.Where(x => x.LotId == lotId).ToList();

				var tords = TransportControl.Manager.GetAllTords();
				tus = tus.Where(x => !tords.Any(t => t.Tu.Name == x.Name)).ToList();

				var tu = tus.FirstOrDefault();

				if (tu == null)
					throw new TransportUnitNotFoundException();

				var loc = TopologyService.Manager.GetDeliveryLocation();

				TransportControl.Manager.CreateTransportOrder(tu, loc);

				return tu;
			}
			catch (Exception e)
			{
				($"Error dispatching transport unit.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		private TransportUnit CreateNewTu(string name, string lotId, string materialId, bool isObserved)
		{
			Location location = TopologyService.Manager.GetReceptionLocation();
			Material material = MaterialService.Manager.GetById(materialId);

			if (material == null) throw new MaterialNotFoundException(name);

			var da = DataAccessRegister.GetDataAccess<TransportUnit>();
			var tu = da.GetElementById(name);

			if (tu != null) throw new TransportUnitAlreadyExistsException(name);

			tu = new TransportUnit();
			tu.Name = name;
			tu.IsObserved = isObserved;
			tu.Location = location;
			tu.LotId = lotId;
			tu.Material = material;

			da.InsertElement(tu);

			return tu;
		}

		public void ObserveTransportUnit(string name, bool isObserved)
		{
			try
			{
				($"Observe Transport Unit name [{name}], IsObserved [{isObserved}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TransportUnit>();
				var tu = da.GetElementById(name);

				if (tu == null) throw new TransportUnitNotFoundException();

				tu.IsObserved = isObserved;

				da.UpdateElement(tu);
			}
			catch (Exception e)
			{
				($"Error observing transport unit.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteTU(string name)
		{
			try
			{
				($"Deleting transport order. Id [{name}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<TransportUnit>();
				da.DeleteElement(name);
			}
			catch (Exception e)
			{
				($"Error deleting transport order. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}
	}
}
