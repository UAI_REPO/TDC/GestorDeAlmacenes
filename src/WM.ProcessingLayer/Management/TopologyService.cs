﻿using Services.Contracts;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using WM.Common.Entities;
using WM.DataAccess;
using WM.ProcessingLayer.Contracts;

namespace WM.ProcessingLayer.Management
{
	public class TopologyService : ITopologyService
	{
		#region Singleton
		private static object lockObj = new object();

		private static ITopologyService service;
		public static ITopologyService Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new TopologyService();

					return service;
				}
			}
		}
		#endregion

		public IList<Location> GetAllLocations(string prop = null, object value = null)
		{
			try
			{
				($"Getting all locations. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Location>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all locations. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public Location GetByName(string name)
		{
			try
			{
				($"Get location by name [{name}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Location>();
				return da.GetElementById(name);
			}
			catch (Exception e)
			{
				($"Error getting location by name [{name}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public Location GetDeliveryLocation()
		{
			try
			{
				($"Getting location for delivery").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Location>();
				return da.GetAllElements(nameof(Location.Class), "Delivery").FirstOrDefault();
			}
			catch (Exception e)
			{
				($"Error getting location for delivery").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public Location GetReceptionLocation()
		{
			try
			{
				($"Getting location for reception").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Location>();
				return da.GetAllElements(nameof(Location.Class), "Reception").FirstOrDefault();
			}
			catch (Exception e)
			{
				($"Error getting location for reception").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void InsertLocation(Location location)
		{
			try
			{
				($"Insert new Location name [{location.Name}], type [{location.Type}], W:{location.W} B:{location.B} A:{location.A} X:{location.X} Y:{location.Y} Z:{location.Z}").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Location>();
				var loc = da.GetElementById(location.Name);

				if (loc != null) throw new Exception("Location already exists");

				da.InsertElement(location);
			}
			catch (Exception e)
			{
				($"Error inserting new location.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteLocation(string name)
		{ 
			try
			{
				($"Delete location Name [{name}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Location>();
				da.DeleteElement(name);
			}
			catch (Exception e)
			{
				($"Error deleting location Name [{name}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdateLocation(Location location)
		{
			try
			{
				($"update Location name [{location.Name}], type [{location.Type}], W:{location.W} B:{location.B} A:{location.A} X:{location.X} Y:{location.Y} Z:{location.Z}").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Location>();
				
				da.UpdateElement(location);
			}
			catch (Exception e)
			{
				($"Error updating location.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}
	}
}
