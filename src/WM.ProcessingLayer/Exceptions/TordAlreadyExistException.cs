﻿using Services.Contracts.Extensions;
using System;
using System.Runtime.Serialization;

namespace WM.ProcessingLayer.Exceptions
{
	public class TordAlreadyExistException : Exception
	{
		public TordAlreadyExistException(string name) : base(string.Format($"TordAlreadyExistException".Translate(), name))
		{

		}
	}
}