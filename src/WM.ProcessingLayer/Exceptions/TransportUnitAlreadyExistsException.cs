﻿using Services.Contracts.Extensions;
using System;
using System.Runtime.Serialization;

namespace WM.ProcessingLayer.Exceptions
{
	public class TransportUnitAlreadyExistsException : Exception
	{
		public TransportUnitAlreadyExistsException(string name) : base(string.Format($"TransportUnitAlreadyExistsException".Translate(), name))
		{

		}
	}
}