﻿using Services.Contracts.Extensions;
using System;

namespace WM.ProcessingLayer.Exceptions
{
	public class UserNotFoundException : Exception
	{
		public UserNotFoundException()
		{
		}

		public UserNotFoundException(string user) : base(string.Format($"UserNotFoundException".Translate(), user))
		{

		}

	}
}