﻿using Services.Contracts.Extensions;
using System;
using System.Runtime.Serialization;

namespace WM.ProcessingLayer.Exceptions
{
	public class LocationNotFoundException : Exception
	{
		public LocationNotFoundException()
		{
		}

		public LocationNotFoundException(string name) : base(string.Format($"LocationNotFoundException".Translate(), name))
		{

		}
	}
}