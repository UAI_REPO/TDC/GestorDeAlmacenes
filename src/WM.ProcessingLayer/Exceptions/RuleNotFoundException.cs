﻿using Services.Contracts.Extensions;
using System;
using System.Runtime.Serialization;

namespace WM.ProcessingLayer.Exceptions
{
	public class RuleNotFoundException : Exception
	{
		public RuleNotFoundException(string name) : base(string.Format($"RuleNotFoundException".Translate(), name))
		{

		}
	}
}