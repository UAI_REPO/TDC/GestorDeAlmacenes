﻿using Services.Contracts.Extensions;
using System;

namespace WM.ProcessingLayer.Exceptions
{
	public class MaterialNotFoundException : Exception
	{
		public MaterialNotFoundException()
		{
		}

		public MaterialNotFoundException(string name) : base(string.Format($"MaterialNotFoundException".Translate(), name))
		{

		}
	}
}