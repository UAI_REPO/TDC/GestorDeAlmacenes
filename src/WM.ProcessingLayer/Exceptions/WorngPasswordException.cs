﻿using Services.Contracts.Extensions;
using System;
using System.Runtime.Serialization;

namespace WM.ProcessingLayer.Exceptions
{
	public class WorngPasswordException : Exception
	{
		public WorngPasswordException() : base($"WorngPasswordException".Translate())
		{
		}

	}
}