﻿using Services.Contracts.Extensions;
using System;
using System.Runtime.Serialization;

namespace WM.ProcessingLayer.Exceptions
{
	public class TransportUnitNotFoundException : Exception
	{
		public TransportUnitNotFoundException() : base($"TUNotFoundException".Translate())
		{
		}

		public TransportUnitNotFoundException(string name) : base(string.Format($"TransportUnitNotFoundException".Translate(), name))
		{

		}
	}
}