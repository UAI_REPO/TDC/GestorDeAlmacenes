﻿using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Management;

namespace WM.ProcessingLayer.Job
{
	public class AutomaticMovementsJob
	{
		string path = ConfigurationManager.AppSettings["AutomaticMovementPath"];

		public async Task Start()
		{
			"Starting Job".LogInfo();
			var interval = string.IsNullOrEmpty(ConfigurationManager.AppSettings["JobInterval"]) ? 10000 : Convert.ToInt32(ConfigurationManager.AppSettings["JobInterval"]);
			
			while (true)
			{
				try
				{
					"Reading files...".LogInfo();

					var tords = GetMovementsForFiles();

					foreach (var tord in tords)
					{
						$"Creating tord... Tu [{tord.Tu.Name}] Target [{tord.TargetLocation.Name}]".LogInfo();
						TransportControl.Manager.CreateTransportOrder(tord.Tu.Name, tord.TargetLocation.Name);
					}

					ClearMovements();
				}
				catch (Exception e)
				{
					"Error processing automatic movements".LogError(e);
				}
				finally
				{
					await Task.Delay(interval);
				}
			}

		}

		private void ClearMovements()
		{
			var files = Directory.GetFiles(path);
			foreach (var file in files)
			{
				File.Delete(file);
			}
		}

		private IList<TransportOrder> GetMovementsForFiles()
		{
			if (!Directory.Exists(path))
				Directory.CreateDirectory(path);

			var files = Directory.GetFiles(path);

			var result = new List<TransportOrder>();
			foreach (var file in files)
			{
				DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(IList<TransportOrder>));
				using (FileStream f = new FileStream($"{file}", FileMode.Open, FileAccess.Read))
				{
					var items = (IList<TransportOrder>)js.ReadObject(f);
					result.AddRange(items);
				}
			}

			return result;
		}
	}
}
