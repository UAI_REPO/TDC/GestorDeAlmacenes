﻿using System;
using System.Collections.Generic;
using WM.Common.Entities;

namespace WM.ProcessingLayer.Management
{
	public interface ITransportMovementService
	{
		void CreateMovement(TransportOrder tord, Location loc);

		public IList<TordMovement> GetAllMovements(string prop = null, object value = null, Type entity = null);

		public void Pickup(string tu);

		public void Deposit(string tu, string location);

		IList<TordMovementHist> GetAllMovementsHist(string prop = null, object value = null, Type entity = null);

		void GenerateMovementsReport(string path);

		void SerializeMovements(string path);
	}
}