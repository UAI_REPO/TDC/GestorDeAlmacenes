﻿using System.Collections.Generic;
using WM.Common.Entities;

namespace WM.ProcessingLayer.Contracts
{
	public interface IMaterialService
	{
		Material GetById(string name);

		IList<Material> GetAllMaterials(string prop = null, object value = null);
		void InsertMaterial(Material material);
		void UpdateMaterial(Material material);
		void DeleteMaterial(string name);
	}
}