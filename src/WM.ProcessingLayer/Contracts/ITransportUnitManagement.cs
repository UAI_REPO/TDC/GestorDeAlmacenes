﻿using System;
using System.Collections.Generic;
using WM.Common.Entities;

namespace WM.ProcessingLayer.Contracts
{
	public interface ITransportUnitManagement
	{
		IList<TransportUnit> GetAllTus(string prop = null, object value = null, Type entity = null);

		TransportUnit GetByName(string name);

		TransportUnit ReceiveTu(string lotId, string materialId, bool isObserved = false, string name = null);

		void DeleteTU(string name);

		void ObserveTransportUnit(string name, bool isObserved);

		void MoveTu(TransportUnit tu, Location location);

		TransportUnit DeliveryTu(string name, string lotId);
	}
}