﻿using System.Collections.Generic;
using WM.Common.Entities;

namespace WM.ProcessingLayer.Management.SearchRules
{
	public interface IRule
	{
		IList<Location> GetLocations(TransportUnit tu, LocationType level, IList<Location> parents, Rule rule);
	}
}