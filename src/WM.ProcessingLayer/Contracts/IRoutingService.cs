﻿using System;
using System.Collections.Generic;
using WM.Common.Entities;

namespace WM.ProcessingLayer.Contracts
{
	public interface IRoutingService
	{
		Location GetRoutesFor(string tuName, string location);
		IList<Route> GetAllRoutes(string prop = null, object value = null, Type entity = null);
		void InsertRoute(Route route);
		void UpdateRoute(Route route);
		void DeleteRoute(long id);
	}
}