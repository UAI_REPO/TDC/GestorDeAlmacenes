﻿using System;
using System.Collections.Generic;
using WM.Common.Entities;

namespace WM.ProcessingLayer.Contracts
{
	public interface IRulesManagement
	{
		Location GetSuitableLocationFor(TransportUnit tu, Location parent = null);

		IList<Rule> GetAllRules(string prop = null, object value = null, Type entity = null);

		void InsertRule(Rule rule);

		void UpdateRule(Rule rule);

		void DeleteRule(long id);

		IList<ParameterizedDetail> GetAllParameterizedRules(string prop = null, object value = null, Type entity = null);

		void InsertParameterizedRule(ParameterizedDetail rule);

		void UpdateParameterizedRule(ParameterizedDetail rule);

		void DeleteParameterizedRule(long id);
	}
}