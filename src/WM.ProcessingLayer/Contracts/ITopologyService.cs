﻿using System.Collections.Generic;
using System.Reflection;
using WM.Common.Entities;

namespace WM.ProcessingLayer.Contracts
{
	public interface ITopologyService
	{
    IList<Location> GetAllLocations(string prop = null, object value = null);

    void InsertLocation(Location location);

    void UpdateLocation(Location location);

    void DeleteLocation(string name);

    Location GetByName(string name);

    Location GetReceptionLocation();

    Location GetDeliveryLocation();
  }
}
