﻿using System;
using System.Collections.Generic;
using WM.Common.Entities;

namespace WM.ProcessingLayer.Contracts
{
	public interface ITransportControl
	{
		void ChangeTordStatus(TransportOrder tord, Location loc);

		void CreateTransportOrder(TransportUnit tu, Location target);

		void CreateTransportOrder(string tuname, string target);

		IList<TransportOrder> GetAllTords(string prop = null, object value = null, Type entity = null);

		void DeleteTord(long id);

		IList<TransportOrderHist> GetAllTordsHist(string prop = null, object value = null, Type entity = null);

	}
}