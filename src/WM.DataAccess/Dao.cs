﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using Services.Contracts;
using Services.Contracts.Extensions;
using WM.Common.Entities;
using WM.Common.Exceptions;
using WM.Common.Helper;

namespace WM.DataAccess
{
	public class Dao<T> where T : new()
	{
		protected string connectionString;

		public Dao()
		{
			connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
		}

		public virtual IList<T> GetAllElements(string filter = null)
		{
			return GetElementsByQuery(filter);
		}

		public virtual T GetElementById(object id)
		{
			var filter = string.Empty;
			if (typeof(T).GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name == DataModel.Identifier)
				filter += $" [{typeof(T).Name}].Id = {ConvertMetaDataToString(id)}";
			else
				filter += $" [{typeof(T).Name}].{typeof(T).GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name} = {ConvertMetaDataToString(id)}";

			return GetElementsByQuery(filter, true).FirstOrDefault();
		}

		public virtual IList<T> GetElementsByQuery(string filter = null, bool selectFirst = false)
		{
			try
			{
				var result = new List<T>();

				var modelNamespaces = GetModelNamespaces();

				var columnsNames = new List<string>();
				var joinStatements = new List<string>();
				PopulateSelectCommand(typeof(T), columnsNames, joinStatements);


				var cmd = @$"SELECT {(selectFirst ? "TOP(1)" : string.Empty)} {string.Join(",\n", columnsNames)} 
												FROM [{typeof(T).Name}] 
												{(joinStatements.Count > 0 ? string.Join(' ', joinStatements) : string.Empty)}
												{(!string.IsNullOrEmpty(filter) ? $" WHERE {filter}" : string.Empty)}";

				cmd = Regex.Replace(cmd, @"\t|\r", "");

				using (var connection = new SqlConnection(connectionString))
				{
					connection.Open();
					using (var command = new SqlCommand(cmd, connection))
					{
						var reader = command.ExecuteReader();
						
						var columns = new List<string>();

						for (int i = 0; i < reader.FieldCount; i++)
						{
							columns.Add(reader.GetName(i));
						}

						while (reader.Read())
						{
							//var item = new T();

							var data = new List<KeyValuePair<string[], object>>();
							for (int i = 0; i < reader.FieldCount; i++)
							{
								columns.Add(reader.GetName(i));
								data.Add(new KeyValuePair<string[], object>(reader.GetName(i).Split('_'), reader[i]));
							}

							var item = PopulateItem(typeof(T), data);
							result.Add((T)item);
						}

						return result;
					}
				}
			}
			catch (Exception e)
			{
				$"Error processing select command on database.".LogError(e);
				ServiceContract.ManageException(e);
				throw;
			}

		}

		private object PopulateItem(object basetype, List<KeyValuePair<string[], object>> data)
		{
			PropertyInfo propertyInfo = basetype is PropertyInfo ? (PropertyInfo)basetype : null;
			Type type = propertyInfo == null ? (Type)basetype : propertyInfo.PropertyType;
			var typeName = propertyInfo != null ? propertyInfo.Name : type.Name;

			var modelNamespaces = GetModelNamespaces();

			data = data.Where(x => x.Value != DBNull.Value).ToList();

			var entityColumns = data.Where(x => x.Key.Length == 2 && x.Key[0] == typeName);

			var item = Activator.CreateInstance(type);

			//SET PROPERTIES
			foreach (var dataColumn in entityColumns)
			{
				var col = dataColumn.Key[1];
				var value = dataColumn.Value;

				PropertyInfo prop = item.GetType().GetProperty(col, BindingFlags.Public | BindingFlags.Instance);
				if (prop != null && prop.CanWrite)
					SetObjectValue(prop, item, value);
			}

			//remove all elements of this level
			foreach (var datacolumn in data.Where(x => x.Key.Length == 2).ToList())
			{
				data.Remove(datacolumn);
			}

			data = data.Select(x => new KeyValuePair<string[], object>(x.Key.Skip(1).ToArray(), x.Value)).ToList();

			//CALL RECURSIVE FUNCTION
			foreach (var property in type.GetProperties().Where(x => modelNamespaces.Contains(x.PropertyType.Namespace) && x.PropertyType.BaseType.Name != nameof(Enum)))
			{
				property.SetValue(item, PopulateItem(property, data));
			}

			return item;
		}

		private void PopulateSelectCommand(object basetype, List<string> columnsNames, List<string> joinStatements, string baseEntityName = null)
		{
			
			var modelNamespaces = GetModelNamespaces();

			PropertyInfo propertyInfo = basetype is PropertyInfo ? (PropertyInfo)basetype: null;
			Type type = propertyInfo == null ? (Type)basetype : propertyInfo.PropertyType;

			if (basetype is Type)
				type = (Type)basetype;
			else
				type = ((PropertyInfo)basetype).PropertyType;
			

			baseEntityName += $"{(!string.IsNullOrEmpty(baseEntityName) ? "_" : string.Empty)}{(propertyInfo != null ? propertyInfo.Name : type.Name)}";

			foreach (var property in type.GetProperties())
			{
				if (property.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
					continue;

				if (modelNamespaces.Contains(property.PropertyType.Namespace) && property.PropertyType.BaseType.Name != nameof(Enum))
				{
					var propId = property.PropertyType.GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute)));
					joinStatements.Add($"LEFT JOIN [{property.PropertyType.Name}] as [{baseEntityName}_{property.Name}] ON {$"[{baseEntityName}]."}{property.Name}{propId.Name} = [{baseEntityName}_{property.Name}].{propId.Name} \n");
					PopulateSelectCommand(property, columnsNames, joinStatements, $"{baseEntityName}");
				}
				else
				{
					columnsNames.Add($"[{baseEntityName}].{property.Name} {baseEntityName}_{property.Name}");
				}
			}
		}

		private IList<string> GetModelNamespaces()
		{
			var settings = ConfigurationManager.AppSettings;
			return settings["MapperNamepaces"].Split(";").ToList();
		}

		public virtual IList<T> GetAllElements(string propName, object value, Type entity = null)
		{
			try
			{
				string filter = string.Empty;
				if (entity == null)
				{
					var prop = typeof(T).GetProperties().FirstOrDefault(x => x.Name == propName);
					filter = $"[{typeof(T).Name}].{prop.Name} = {ConvertMetaDataToString(prop, value)}";
				}
				else
				{ 
					var propEntity = typeof(T).GetProperties().FirstOrDefault(x => x.PropertyType.Name == entity.Name);
					var prop = entity.GetProperties().FirstOrDefault(x => x.Name == propName);
					filter = $"[{typeof(T).Name}_{propEntity.Name}].{prop.Name} = {ConvertMetaDataToString(prop, value)}";
				}
				
				return GetAllElements(filter);
			}
			catch (Exception e)
			{
				$"Error processing select command on database.".LogError(e);
				ServiceContract.ManageException(e);
				throw;
			}

		}

		public virtual void InsertElement(T entity)
		{
			try
			{
				var modelNamespaces = GetModelNamespaces();

				//var id = entity.GetType().GetProperties().FirstOrDefault(x => x.Name == DataModel.Identifier);
				var id = entity.GetType().GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute)));

				var properties = entity.GetType().GetProperties();

				if (id != null && id.PropertyType != typeof(string))
					properties = properties.Where(x => x.Name != id.Name).ToArray();

				var data = new Dictionary<string, object>();
				foreach (var property in properties)
				{
					if (property.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
						continue;

					if (modelNamespaces.Contains(property.PropertyType.Namespace) && property.PropertyType.BaseType.Name != nameof(Enum))
					{
						var obj = property.GetValue(entity);

						if (obj == null)
							continue;

						var objIdValue = obj.GetType().GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).GetValue(obj);
						data.Add(property.Name + obj.GetType().GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name, objIdValue);
					}
					else
					{
						data.Add(property.Name, property.GetValue(entity));
					}
				}

				var values = data.Values.Select(ConvertMetaDataToString);

				var cmd = $"INSERT INTO [{entity.GetType().Name}] ({string.Join(',', data.Keys)}) VALUES ({string.Join(',', values)});";

				if (id != null && id.PropertyType != typeof(string))
					cmd += " SELECT CAST(SCOPE_IDENTITY() AS " + (id.PropertyType == typeof(long) ? "BIGINT" : "INT") + ");";

				using (var connection = new SqlConnection(connectionString))
				{
					connection.Open();

					using (var command = new SqlCommand(cmd, connection))
					{
						if (id != null && id.PropertyType != typeof(string))
						{
							var result = command.ExecuteScalar();
							id.SetValue(entity, result);
						}
						else
						{
							command.ExecuteNonQuery();
						}
					}
				}

			}
			catch (Exception e)
			{
				$"Error processing insert command on database.".LogError(e);
				ServiceContract.ManageException(e);
				throw;
			}

		}

		public virtual void UpdateElement(T entity)
		{
			try
			{
				var modelNamespaces = GetModelNamespaces();

				var cmd = $@"UPDATE [dbo].[{typeof(T).Name}] SET ";
				var propertyStatements = new List<string>();
				//long idValue = 0;
				string idValue = string.Empty;
				foreach (var property in typeof(T).GetProperties())
				{
					if (property.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
						continue;

					var value = property.GetValue(entity);
					
					if (property.Name == typeof(T).GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name)
					{
						idValue = value.ToString();
						continue;
					}

					if (modelNamespaces.Contains(property.PropertyType.Namespace) && property.PropertyType.BaseType.Name != nameof(Enum))
					{
						var propId = property.PropertyType.GetProperties()
								.FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute)));
						
						var propIdValue = value != null ? propId.GetValue(value): null;
						propertyStatements.Add($"[{property.Name}{property.PropertyType.GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name}] = {ConvertMetaDataToString(propIdValue)} ");
					}
					else
					{
						propertyStatements.Add($"[{property.Name}] = {ConvertMetaDataToString(value)} ");
					}
				}

				cmd += string.Join(',', propertyStatements);

				if(typeof(T).GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name == DataModel.Identifier)
					cmd += $"WHERE id = {ConvertMetaDataToString(idValue)}";
				else
					cmd += $"WHERE {typeof(T).GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name} = {ConvertMetaDataToString(idValue)}";

				using (var connection = new SqlConnection(connectionString))
				{
					connection.Open();

					using (var command = new SqlCommand(cmd, connection))
					{
						command.ExecuteNonQuery();
					}
				}

			}
			catch (Exception e)
			{
				$"Error processing update command on database.".LogError(e);
				ServiceContract.ManageException(e);
				throw;
			}
		}

		public virtual void DeleteElement(object id)
		{
			try
			{
				var cmd = $@"DELETE FROM [dbo].[{typeof(T).Name}] ";

				if (typeof(T).GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name == DataModel.Identifier)
					cmd += $"WHERE id = {ConvertMetaDataToString(id)}";
				else
					cmd += $"WHERE {typeof(T).GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name} = {ConvertMetaDataToString(id)}";

				using (var connection = new SqlConnection(connectionString))
				{
					connection.Open();

					using (var command = new SqlCommand(cmd, connection))
					{
						command.ExecuteNonQuery();
					}
				}

			}
			catch (Exception e)
			{
				$"Error processing delete command on database.".LogError(e);
				ServiceContract.ManageException(e);
				throw;
			}
		}

		#region Helpers

		private static void SetObjectValue(PropertyInfo prop, object item, object value)
		{

			//Check Enum
			if (prop.PropertyType is Enum)
			{
				prop.SetValue(item, Enum.ToObject(prop.PropertyType, (int)value), null);
				return;
			}

			var nullableType = Nullable.GetUnderlyingType(prop.PropertyType);
			if (nullableType != null && nullableType.IsEnum)
			{
				prop.SetValue(item, Enum.ToObject(nullableType, (int)value), null);
				return;
			}

			prop.SetValue(item, value, null);
		}

		private string ConvertMetaDataToString(object t)
		{
			if (t == null)
				return "null";

			if (t is DateTime)
				return $"'{(DateTime)t:s}'";

			if (t is string)
				return $"'{t}'";

			if (t is bool)
				return $"'{t}'";

			if (t.GetType().BaseType.Name == nameof(Enum))
			{
				object underlyingValue = Convert.ChangeType(t, Enum.GetUnderlyingType(t.GetType()));
				return underlyingValue.ToString();
			}

			return t.ToString();
		}

		private string ConvertMetaDataToString(PropertyInfo type, object value)
		{
			if (value == null)
				return "null";

			if (type.PropertyType.Name == nameof(DateTime))
				return $"'{(DateTime)value:s}'";

			if (type.PropertyType.Name == nameof(String))
				return $"'{value}'";

			if (type.PropertyType.Name == nameof(Boolean))
				return $"'{value}'";

			if (type.PropertyType.BaseType.Name == nameof(Enum) && value is string)
			{
				return ((int)Enum.Parse(type.PropertyType, value.ToString())).ToString();
			}

			return value.ToString();
		}
		#endregion
	}
}
