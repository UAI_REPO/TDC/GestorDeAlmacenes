﻿using System;
using System.Reflection;

namespace WM.DataAccess
{
	public static class DataAccessRegister
	{
		public static Dao<T> GetDataAccess<T>() where T : new()
		{
			return new Dao<T>();
		}
	}
}
