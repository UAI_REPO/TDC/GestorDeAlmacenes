﻿using Newtonsoft.Json;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace Services.Mangement
{
	internal class Regionalization
	{
		#region Singleton
		private static object lockObj = new object();

		private static Regionalization service;
		public static Regionalization Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new Regionalization();

					return service;
				}
			}
		}
		#endregion

		Dictionary<string, string> regionalization;

		public Regionalization()
		{
			regionalization = new Dictionary<string, string>();
		}

		public void Load(string culture = null)
		{
			try
			{
				if(string.IsNullOrEmpty(culture))
					culture = "es-AR";

				string jsonString = File.ReadAllText($"./I18n/{culture}.json", Encoding.UTF7);
				regionalization = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonString);
			}
			catch (Exception e)
			{
				ServiceContract.WriteLog(Entities.Logger.LoggerLevel.Error, e, $"Error trying to load Localization file.");
			}
		}

		public string Transalte(string word)
		{
			if (regionalization.ContainsKey(word))
				return regionalization[word];

			return word;
		}
	}
}
