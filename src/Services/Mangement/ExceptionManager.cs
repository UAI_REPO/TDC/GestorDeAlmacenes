﻿using Services.Entities.Exceptions;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Services.Mangement
{
	internal sealed class ExceptionManager
	{
		#region Singleton
		private static object lockObj = new object();

		private static ExceptionManager service;
		public static ExceptionManager Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new ExceptionManager();

					return service;
				}
			}
		}
		#endregion

		/// <summary>
		/// Process exception component
		/// Rules:
		/// Include the exception name (DAO, BLL or UI) into APP.Config 
		/// with the namespace of the level (it will be more that one namespace).
		/// eg:
		///		key="ProcessingLayerException" value="WM.ProcessingLayer;WM.TopologyService;"
		///		key="DataAccessException" value="WM.DataAccess;"
		///	
		/// if an exception comes to this component with the same type of this excections 
		/// it throw it without changes to keep the message and bubble it to the top level.
		/// In other way, the component try to find the exception level using the source of 
		/// the exception and matching it with the value of the keys (namespaces). Finally
		/// a new exception is created based on this information.
		/// </summary>
		/// <param name="ex">The base exception</param>
		public void Process(Exception ex)
		{
			var settings = ConfigurationManager.AppSettings;

			if (settings[ex.GetType().Name] != null)
			{
				throw ex;
			}

			var key = settings.AllKeys.FirstOrDefault(k => settings[k].Split(";").Any(x => x == ex.Source));
			if (key != null)
			{
				if (settings["CustomsExceptions"].Split(";").Any(x => x == ex.GetType().Namespace))
					throw ex;

				var newEx = Activator.CreateInstance(Type.GetType($"Services.Entities.Exceptions.{key}"), new object[] { ex });
				throw (Exception)newEx;
			}
			else
			{
				//if(ex.InnerException != null)
				//	Process(ex.InnerException)
				throw new Exception("Ocurrio un error inesperado con el sistema, contacte al administrador", ex);
			}
		}
	}
}
