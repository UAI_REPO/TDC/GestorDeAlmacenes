﻿using Microsoft.CodeAnalysis;
using Serilog;
using Services.Entities.Exceptions;
using Services.Entities.Logger;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Services.Mangement
{
	internal sealed class LoggerManager
	{
		#region Singleton
		private static object lockObj = new object();

		private static LoggerManager service;
		public static LoggerManager Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new LoggerManager();

					return service;
				}
			}
		}
		#endregion


		private static Serilog.ILogger _logger;
		public LoggerManager()
		{
			_logger = new LoggerConfiguration()
									.MinimumLevel.Debug()
									.WriteTo.Console()
									.WriteTo.File("Logs/log-.txt", rollingInterval: RollingInterval.Day, outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} | {ThreadId} | {Level:u3} | {SourceContext} | {Message:lj}{NewLine}{Exception}")
									.Enrich.FromLogContext()
									.Enrich.WithThreadId()
									.CreateLogger();
		}

		public void Write(LoggerLevel severity, string message)
		{
			switch (severity)
			{
				case LoggerLevel.Debug:
					_logger.Debug(message);
					break;
				case LoggerLevel.Info:
					_logger.Information(message);
					break;
				case LoggerLevel.Warning:
					_logger.Warning(message);
					break;
				case LoggerLevel.Error:
					_logger.Error(message);
					break;
				default:
					_logger.Fatal(message);
					break;
			}
		}

		public void Write(LoggerLevel severity, Exception e, string message)
		{
			switch (severity)
			{
				case LoggerLevel.Debug:
					_logger.Debug(e, message);
					break;
				case LoggerLevel.Info:
					_logger.Information(e, message);
					break;
				case LoggerLevel.Warning:
					_logger.Warning(e, message);
					break;
				case LoggerLevel.Error:
					_logger.Error(e, message);
					break;
				default:
					_logger.Fatal(e, message);
					break;
			}
		}
	}
}
