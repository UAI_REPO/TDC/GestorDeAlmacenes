﻿using Newtonsoft.Json;
using Services.Contracts;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Services.Mangement
{
	internal class DBManager
	{
		#region Singleton
		private static object lockObj = new object();

		private static DBManager service;
		public static DBManager Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new DBManager();

					return service;
				}
			}
		}
		#endregion


		protected string connectionString;

		public DBManager()
		{
			connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
		}

		internal void CheckOrInitializeDB()
		{
			try
			{
				var connItems = connectionString.Split(";").ToList();
				var item = connItems.First(x => x.StartsWith("Initial Catalog="));
				var index = connItems.IndexOf(item);
				connItems[index] = "Initial Catalog=master";

				var currentDb = item.ToString().Remove(0, 16);

				var masterConn = string.Join(';', connItems);

				string cmdText = $"select * from master.dbo.sysdatabases where name=\'{currentDb}\'";

				using (SqlConnection sqlConnection = new SqlConnection(masterConn))
				{
					sqlConnection.Open();

					using (SqlCommand sqlCmd = new SqlCommand(cmdText, sqlConnection))
					{
						SqlDataReader reader = sqlCmd.ExecuteReader();
						if (reader.HasRows)
							return;
					}
				}
				RestoreDB(ConfigurationManager.AppSettings["DBInitializationName"], false);
			}
			catch (Exception e)
			{
				($"Error initialize DB. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		internal void DeleteBackup(string name)
		{
			try
			{
				var path = @$"{Directory.GetCurrentDirectory()}\Backups\{name}";

				File.Delete(path);
			}
			catch (Exception e)
			{
				$"Error deleting backup.".LogError(e);
				ServiceContract.ManageException(e);
				throw;
			}
		}

		internal IList<FileInfo> GetBackupList()
		{
			try
			{
				var path = @$"{Directory.GetCurrentDirectory()}\Backups\";

				return Directory.GetFiles(path).Select(x => new FileInfo(x)).ToList();
			}
			catch (Exception e)
			{
				$"Error processing restore.".LogError(e);
				ServiceContract.ManageException(e);
				throw;
			}
		}

		internal void BackupDB(string file)
		{
			try
			{
				var connItems = connectionString.Split(";").ToList();
				var item = connItems.First(x => x.StartsWith("Initial Catalog="));
				var index = connItems.IndexOf(item);
				connItems[index] = "Initial Catalog=master";

				var currentDb = item.ToString().Remove(0, 16);

				var masterConn = string.Join(';', connItems);

				string cmdText = $"select * from master.dbo.sysdatabases where name=\'{currentDb}\'";

				using (SqlConnection sqlConnection = new SqlConnection(masterConn))
				{
					sqlConnection.Open();

					var path = Directory.GetCurrentDirectory();

					string restorecmd = @$"BACKUP DATABASE [{currentDb}] TO DISK = N'{path}\Backups\{file}' WITH NOFORMAT, NOINIT,  NAME = N'WMS.Database-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10";

					using (SqlCommand sqlCmd = new SqlCommand(restorecmd, sqlConnection))
					{
						sqlCmd.CommandTimeout = 1000;
						if (sqlCmd.ExecuteNonQuery() >= 0)
							return;
					}
				}
			}
			catch (Exception e)
			{
				$"Error processing backup.".LogError(e);
				ServiceContract.ManageException(e);
				throw;
			}			
		}

		internal void RestoreDB(string file, bool dbExist = true)
		{
			try
			{
				var connItems = connectionString.Split(";").ToList();
				var item = connItems.First(x => x.StartsWith("Initial Catalog="));
				var index = connItems.IndexOf(item);
				connItems[index] = "Initial Catalog=master";

				var currentDb = item.ToString().Remove(0, 16);

				var masterConn = string.Join(';', connItems);

				string cmdText = $"select * from master.dbo.sysdatabases where name=\'{currentDb}\'";

				using (SqlConnection sqlConnection = new SqlConnection(masterConn))
				{
					sqlConnection.Open();

					var path = Directory.GetCurrentDirectory();
					string restorecmd = dbExist ?  $"ALTER DATABASE[{currentDb}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE \n" : string.Empty;
					
					restorecmd += @$"
							RESTORE DATABASE [{currentDb}] FROM  DISK = N'{path}\Backups\{file}' WITH REPLACE, FILE = 1, 
							--MOVE N'WMS.Database' TO N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\{currentDb}.mdf',  
							--MOVE N'WMS.Database_log' TO N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\{currentDb}6_log.ldf',  
							--MOVE N'WMS.Database' TO N'{path}\{currentDb}.mdf',  
							--MOVE N'WMS.Database_log' TO N'{path}\{currentDb}_log.ldf',
							NOUNLOAD,  STATS = 5
							ALTER DATABASE [{currentDb}] SET MULTI_USER
					";

					using (SqlCommand sqlCmd = new SqlCommand(restorecmd, sqlConnection))
					{
						sqlCmd.CommandTimeout = 1000;
						if (sqlCmd.ExecuteNonQuery() >= 0)
							return;
					}
				}
			}
			catch (Exception e)
			{
				$"Error processing restore.".LogError(e);
				ServiceContract.ManageException(e);
				throw;
			}
		}

	}
}
