﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Entities.Logger
{
	public enum LoggerLevel
	{
		Debug,
		Info,
		Warning,
		Error
	}
}
