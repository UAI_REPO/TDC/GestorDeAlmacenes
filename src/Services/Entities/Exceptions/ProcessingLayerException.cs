﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Entities.Exceptions
{
	public class ProcessingLayerException :Exception
	{
		public ProcessingLayerException(Exception baseException):base("Error en el procesamiento de la información", baseException)
		{

		}

		public ProcessingLayerException(string message, Exception baseException) : base(message, baseException)
		{

		}
	}
}
