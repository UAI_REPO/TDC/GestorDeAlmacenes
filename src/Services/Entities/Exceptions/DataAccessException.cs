﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Entities.Exceptions
{
	public class DataAccessException :Exception
	{
		public DataAccessException(Exception baseException) : base("Error en la capa de datos", baseException)
		{

		}

		public DataAccessException(string message, Exception baseException):base(message, baseException)
		{

		}

		public DataAccessException(string message) : base(message)
		{

		}
	}
}
