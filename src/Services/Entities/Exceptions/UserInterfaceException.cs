﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Entities.Exceptions
{
	public class UserInterfaceException :Exception
	{
		public UserInterfaceException(Exception baseException):base("Error en la interfaz de usuario", baseException)
		{

		}

		public UserInterfaceException(string message) : base(message)
		{

		}
	}
}
