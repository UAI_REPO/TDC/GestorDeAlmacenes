﻿using Services.Entities.Logger;
using Services.Mangement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts
{
	public static class ServiceContract
	{

		public static void ManageException(Exception ex)
		{
			ExceptionManager.Manager.Process(ex);
		}
		public static void LoadLocalization(string culture = null)
		{
			Regionalization.Manager.Load(culture);
		}

		public static void WriteLog(LoggerLevel severity, string message)
		{
			LoggerManager.Manager.Write(severity, message);
		}

		public static void WriteLog(LoggerLevel severity, Exception e, string message)
		{
			LoggerManager.Manager.Write(severity, e, message);
		}

		public static void CheckOrInitializeDB()
		{
			DBManager.Manager.CheckOrInitializeDB();
		}

		public static void RestoreDB(string file)
		{
			DBManager.Manager.RestoreDB(file);
		}

		public static void BackupDB(string name)
		{
			DBManager.Manager.BackupDB(name);
		}

		public static IList<FileInfo> GetBackupList()
		{
			return DBManager.Manager.GetBackupList();
		}

		public static void DeleteBackup(string path)
		{
			DBManager.Manager.DeleteBackup(path);
		}
	}
}
