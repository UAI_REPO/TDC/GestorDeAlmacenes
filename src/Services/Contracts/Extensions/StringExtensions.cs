﻿using Services.Mangement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Contracts.Extensions
{
	public static class StringExtensions
	{
		public static string Translate(this string word)
		{
			return Regionalization.Manager.Transalte(word);
		}

		public static void LogDebug(this string word)
		{
			LoggerManager.Manager.Write(Entities.Logger.LoggerLevel.Debug, word);
		}

		public static void LogInfo(this string word)
		{
			LoggerManager.Manager.Write(Entities.Logger.LoggerLevel.Info, word);
		}

		public static void LogWarning(this string word)
		{
			LoggerManager.Manager.Write(Entities.Logger.LoggerLevel.Warning, word);
		}

		public static void LogError(this string word)
		{
			LoggerManager.Manager.Write(Entities.Logger.LoggerLevel.Error, word);
		}

		public static void LogError(this string word, Exception e)
		{
			LoggerManager.Manager.Write(Entities.Logger.LoggerLevel.Error, e, word);
		}

	}
}
