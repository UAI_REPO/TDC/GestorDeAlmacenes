﻿using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;

namespace WM.UserInterface.Views
{
	public class PageRoleView : UserControl
	{
		public PageRoleView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "Role.Id".Translate(), Binding = new Binding("Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Role.RoleDefinitionId".Translate(), Binding = new Binding("RoleDefinitionId") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Role.InheritanceRoleId".Translate(), Binding = new Binding("InheritanceRoleId") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
