﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Data;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;
using System;

namespace WM.UserInterface.Views
{
	public class PageTordMovementHistView : UserControl
	{
		public PageTordMovementHistView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovementHist.Id".Translate(), Binding = new Binding("Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovementHist.TordMovementId".Translate(), Binding = new Binding("TordMovementId") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovementHist.Tu".Translate(), Binding = new Binding("Tu.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovementHist.From".Translate(), Binding = new Binding("Tu.Location.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovementHist.Status".Translate(), Binding = new Binding("Status") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovementHist.Tord".Translate(), Binding = new Binding("Tord.Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovementHist.TargetLocation".Translate(), Binding = new Binding("TargetLocation.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovementHist.UpdatedAt".Translate(), Binding = new Binding("UpdatedAt") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
