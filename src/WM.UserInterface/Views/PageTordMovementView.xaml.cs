﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Data;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;
using System;

namespace WM.UserInterface.Views
{
	public class PageTordMovementView : UserControl
	{
		public PageTordMovementView()
		{
			this.InitializeComponent();
			InitGrid();

			this.Get<Button>("btnPickup").IsEnabled = false;
			this.Get<Button>("btnDeposit").IsEnabled = false;
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovement.Id".Translate(), Binding = new Binding("Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovement.Tu".Translate(), Binding = new Binding("Tu.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovement.From".Translate(), Binding = new Binding("Tu.Location.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovement.Status".Translate(), Binding = new Binding("Status") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovement.Tord".Translate(), Binding = new Binding("Tord.Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TordMovement.TargetLocation".Translate(), Binding = new Binding("TargetLocation.Name") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
