﻿using Avalonia;
using Avalonia.Collections;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Input;
using WM.UserInterface.Helpers;
using WM.UserInterface.Models;

namespace WM.UserInterface.Views
{
	public class UCMenuItem : UserControl
	{

		public static readonly RoutedEvent<MenuItemEventArgs> SelectedItemEvent =
		RoutedEvent.Register<string, MenuItemEventArgs>(nameof(SelectedItem), RoutingStrategies.Bubble);

		// Provide CLR accessors for the event
		public event EventHandler<MenuItemEventArgs> SelectedItem
		{
			add => AddHandler(SelectedItemEvent, value);
			remove => RemoveHandler(SelectedItemEvent, value);
		}

		public static readonly AvaloniaProperty<UIMenuItem> ItemsProperty =
		AvaloniaProperty.Register<UCMenuItem, UIMenuItem>(nameof(Items), notifying: OnChange);

		private static void OnChange(IAvaloniaObject arg1, bool arg2)
		{
			((UCMenuItem)arg1).LoadItems(((UCMenuItem)arg1).Items);
		}

		public UIMenuItem Items
		{
			get => this.GetValue(ItemsProperty);
			set
			{ 
				this.SetValue(ItemsProperty, value);
				LoadItems(value);
			}
		}

		private void LoadItems(UIMenuItem mi)
		{
			if (mi == null)
				return;

			var panel = this.Get<StackPanel>("panel");
			panel.Children.Clear();

			var mainBtn = this.Get<Button>("item");
			//mainBtn = new Button();
			//mainBtn.Name = "item";

			if (mi.UIs != null && mi.UIs.Count > 0)
			{
				//<Button PointerEnter="OnPointerEnter" PointerLeave="OnPointerLeave" Content="Topology" Classes="btn test"/>

				mainBtn.PointerEnter += OnPointerEnter;
				mainBtn.PointerLeave += OnPointerLeave;
				mainBtn.Content = mi.Name.Translate();
				mainBtn.Tag = mi.Name;
				mainBtn.Classes.Add("btn");
				mainBtn.Classes.Add("test");
			}
			else
			{
				//<Button PointerEnter="OnPointerEnter" Click="OnClick" PointerLeave="OnPointerLeave" Content="Rule" Classes="btn"/>

				mainBtn.PointerEnter += OnPointerEnter;
				mainBtn.PointerLeave += OnPointerLeave;
				mainBtn.Click += OnClick;
				mainBtn.Content = mi.Name.Translate();
				mainBtn.Tag = mi.Name;
				mainBtn.Classes.Add("btn");
			}

			foreach (var ui in mi.UIs)
			{
				var btn = new Button();
				btn.Click += OnClick;
				btn.PointerEnter += OnPointerEnter;
				btn.PointerLeave += OnPointerLeave;
				btn.Content = ui.Translate();
				btn.Tag = ui;
				btn.Classes.Add("btn");

				panel.Children.Add(btn);
			}


		}

		public UCMenuItem()
		{
			this.InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		  var panel = this.Get<StackPanel>("panel");
			panel.IsVisible = false;
		}

		private void OnPointerLeave(object sender, PointerEventArgs e)
		{
			var panel = this.Get<StackPanel>("panel");
			panel.IsVisible = false;
		}

		private void OnPointerEnter(object sender, PointerEventArgs e)
		{
			var panel = this.Get<StackPanel>("panel");
			panel.IsVisible = true;
		}

		private void OnClick(object sender, RoutedEventArgs e)
		{
			var btn = ((Button) sender);

			var selectItem = new MenuItemEventArgs
			{
				RoutedEvent = UCMenuItem.SelectedItemEvent,
				UI = btn.Tag.ToString()
			};

			this.RaiseEvent(selectItem);
		}
		
	}
}
