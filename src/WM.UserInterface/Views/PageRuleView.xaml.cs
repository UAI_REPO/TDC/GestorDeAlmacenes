﻿using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;

namespace WM.UserInterface.Views
{
	public class PageRuleView : UserControl
	{
		public PageRuleView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "Rule.Id".Translate(), Binding = new Binding("Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Rule.Name".Translate(), Binding = new Binding("Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Rule.Type".Translate(), Binding = new Binding("Type.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Rule.Level".Translate(), Binding = new Binding("Level") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Rule.Priority".Translate(), Binding = new Binding("Priority") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Rule.Enabled".Translate(), Binding = new Binding("Enabled") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
