﻿using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;
using System.Collections.Generic;
using System.Linq;
using WM.Common.Entities;
using Location = WM.Common.Entities.Location;

namespace WM.UserInterface.Views
{
	public class PageParameterizedView : UserControl
	{
		public PageParameterizedView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "ParameterizedDetail.Id".Translate(), Binding = new Binding("Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "ParameterizedDetail.Rule".Translate(), Binding = new Binding("Rule.Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "ParameterizedDetail.SourceEntity".Translate(), Binding = new Binding("SourceEntity") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "ParameterizedDetail.SourceProperty".Translate(), Binding = new Binding("SourceProperty") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "ParameterizedDetail.SourceValue".Translate(), Binding = new Binding("SourceValue") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "ParameterizedDetail.SourceOperator".Translate(), Binding = new Binding("SourceOperator") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "ParameterizedDetail.TargetEntity".Translate(), Binding = new Binding("TargetEntity") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "ParameterizedDetail.TargetProperty".Translate(), Binding = new Binding("TargetProperty") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "ParameterizedDetail.TargetValue".Translate(), Binding = new Binding("TargetValue") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "ParameterizedDetail.TargetOperator".Translate(), Binding = new Binding("TargetOperator") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}

		private void SelectionSourceChanged(object item, SelectionChangedEventArgs args)
		{
			if (((ComboBox)item).SelectedItem == null)
				return;

			var sourceProp = this.Get<ComboBox>("sourceProp");
			if (((ComboBox)item).SelectedItem.ToString() == nameof(Material))
				sourceProp.Items = new List<string>(typeof(Material).GetProperties().Select(x => x.Name).ToList());
			else if (((ComboBox)item).SelectedItem.ToString() == nameof(TransportUnit))
				sourceProp.Items = new List<string>(typeof(TransportUnit).GetProperties().Select(x => x.Name).ToList());
		}

		private void SelectionTargetChanged(object item, SelectionChangedEventArgs args)
		{
			if (((ComboBox)item).SelectedItem == null)
				return;

			var targetProp = this.Get<ComboBox>("targetProp");
			if (((ComboBox)item).SelectedItem.ToString() == nameof(Location))
				targetProp.Items = new List<string>(typeof(Location).GetProperties().Select(x => x.Name).ToList());
		}
	}
}
