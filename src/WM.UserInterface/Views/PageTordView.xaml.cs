﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Data;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;
using System;

namespace WM.UserInterface.Views
{
	public class PageTordView : UserControl
	{
		public PageTordView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrder.Id".Translate(), Binding = new Binding("Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrder.Tu".Translate(), Binding = new Binding("Tu.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrder.Status".Translate(), Binding = new Binding("Status") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrder.Message".Translate(), Binding = new Binding("Message") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrder.LastLocation".Translate(), Binding = new Binding("LastLocation.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrder.ActualLocation".Translate(), Binding = new Binding("ActualLocation.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrder.TargetLocation".Translate(), Binding = new Binding("TargetLocation.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrder.CreatedBy".Translate(), Binding = new Binding("CreatedBy") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrder.CreatedAt".Translate(), Binding = new Binding("CreatedAt") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrder.UpdatedBy".Translate(), Binding = new Binding("UpdatedBy") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrder.UpdatedAt".Translate(), Binding = new Binding("UpdatedAt") });

		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
