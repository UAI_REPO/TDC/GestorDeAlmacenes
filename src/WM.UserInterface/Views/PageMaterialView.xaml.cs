﻿using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;

namespace WM.UserInterface.Views
{
	public class PageMaterialView : UserControl
	{
		public PageMaterialView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "Material.Name".Translate(), Binding = new Binding("Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Material.Description".Translate(), Binding = new Binding("Description") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
