﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Data;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;
using System;

namespace WM.UserInterface.Views
{
	public class PageLocationView : UserControl
	{
		public PageLocationView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "Location.Name".Translate(), Binding = new Binding("Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Location.Type".Translate(), Binding = new Binding("Type") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Location.Class".Translate(), Binding = new Binding("Class") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Location.Parent".Translate(), Binding = new Binding("Parent") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Location.Capacity".Translate(), Binding = new Binding("Capacity") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Location.W".Translate(), Binding = new Binding("W") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Location.B".Translate(), Binding = new Binding("B") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Location.A".Translate(), Binding = new Binding("A") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Location.X".Translate(), Binding = new Binding("X") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Location.Y".Translate(), Binding = new Binding("Y") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Location.Z".Translate(), Binding = new Binding("Z") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
