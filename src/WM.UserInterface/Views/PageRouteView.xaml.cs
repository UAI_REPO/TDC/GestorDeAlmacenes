﻿using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;

namespace WM.UserInterface.Views
{
	public class PageRouteView : UserControl
	{
		public PageRouteView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "Route.Id".Translate(), Binding = new Binding("Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Route.LocationSource".Translate(), Binding = new Binding("Source.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Route.LocationTarget".Translate(), Binding = new Binding("Target.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Route.LocationNext".Translate(), Binding = new Binding("Next.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "Route.Enabled".Translate(), Binding = new Binding("Enabled") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
