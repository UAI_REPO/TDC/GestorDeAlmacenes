﻿using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;

namespace WM.UserInterface.Views
{
	public class PageUserView : UserControl
	{
		public PageUserView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "User.Id".Translate(), Binding = new Binding("Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "User.Name".Translate(), Binding = new Binding("Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "User.Password".Translate(), Binding = new Binding("Password") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
