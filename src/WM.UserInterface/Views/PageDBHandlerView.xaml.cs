﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Data;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;
using System;

namespace WM.UserInterface.Views
{
	public class PageDBHandlerView : UserControl
	{
		public PageDBHandlerView()
		{
			this.InitializeComponent();
			InitGrid();

			this.Get<Button>("btnRestore").IsEnabled = false;
			this.Get<Button>("btnDelete").IsEnabled = false;
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "DBHandler.Path".Translate(), Binding = new Binding("Path") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "DBHandler.Date".Translate(), Binding = new Binding("Date") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
