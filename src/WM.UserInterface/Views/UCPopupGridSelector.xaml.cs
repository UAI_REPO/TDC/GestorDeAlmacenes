﻿using Avalonia;
using Avalonia.Collections;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Input;
using WM.UserInterface.Helpers;
using WM.UserInterface.Models;
using Avalonia.Data;
using WM.Common.Entities;
using System.Reflection;

namespace WM.UserInterface.Views
{
	public class UCPopupGridSelector : UserControl
	{
		#region Static Properties

		#region Items
		public static readonly DirectProperty<UCPopupGridSelector, IEnumerable> ItemsProperty =
				ItemsControl.ItemsProperty.AddOwner<UCPopupGridSelector>(o => o.Items, (o, v) => o.Items = v);

		private IEnumerable _items = new AvaloniaList<object>();

		public IEnumerable Items
		{
			get { return _items; }
			set
			{
				SetAndRaise(ItemsProperty, ref _items, value);
				if (value != null) LoadGrid();
			}
		}
		#endregion

		#region Selected Item

		public static readonly AvaloniaProperty<object> SelectedItemProperty =
		AvaloniaProperty.Register<UCPopupGridSelector, object>(nameof(SelectedItem), notifying: OnChange);

		private static void OnChange(IAvaloniaObject sender, bool arg2)
		{
			((UCPopupGridSelector)sender).SelectedItem = ((UCPopupGridSelector)sender).SelectedItem;
		}

		public object SelectedItem
		{
			get => this.GetValue(SelectedItemProperty);
			set
			{
				this.SetValue(SelectedItemProperty, value);
				var txt = this.Get<TextBox>("txtSelectedItem");

				if (value == null)
					txt.Text = null;
				else
				{
					var idprod = value.GetType().GetProperties().FirstOrDefault();
					txt.Text = idprod.GetValue(value).ToString();
				}
			}
		}
		#endregion

		#endregion

		#region Constructor
		public UCPopupGridSelector()
		{
			AvaloniaXamlLoader.Load(this);
		}

		#endregion

		private void LoadGrid()
		{
			var grid = this.Get<DataGrid>("popupgrid");

			grid.Columns.Clear();

			var element = ((IEnumerable<object>)Items).FirstOrDefault();

			if (element == null)
				return;

			var type = element.GetType();

			foreach (var prop in type.GetProperties())
			{
				var bindingProp = prop.Name;

				if (prop.PropertyType.Namespace == type.Namespace && prop.PropertyType.BaseType.Name != nameof(Enum))
				{
					var idprop = prop.PropertyType.GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name;
					bindingProp += $".{idprop}";
				}

				grid.Columns.Add(new DataGridTextColumn() { Header = $"{type.Name}.{prop.Name}".Translate(), Binding = new Binding(bindingProp) });
			}

			grid.Items = Items;
			grid.SelectedItem = null;

			grid.Width = type.GetProperties().Count() * 90;
			if (grid.Width < 300)
				grid.Width = 300;

			// HorizontalOffset="360" VerticalOffset="-60" 

			this.Get<Popup>("selectorpopup").Width = grid.Width + 40;

			this.Get<Popup>("selectorpopup").VerticalOffset = -60;
			this.Get<Popup>("selectorpopup").HorizontalOffset = Convert.ToInt32(this.Get<Popup>("selectorpopup").Width / 2);

		}

		private void OnOpenPopupClick(object sender, RoutedEventArgs e)
		{
			var popup = this.Get<Popup>("selectorpopup");
			popup.IsOpen = true;
		}

		private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			SelectedItem = ((DataGrid)sender).SelectedItem;
			var popup = this.Get<Popup>("selectorpopup");
			popup.IsOpen = false;
		}

	}
}
