﻿using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;

namespace WM.UserInterface.Views
{
	public class PageRoleDefinitionView : UserControl
	{
		public PageRoleDefinitionView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "RoleDefinition.Id".Translate(), Binding = new Binding("Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "RoleDefinition.Name".Translate(), Binding = new Binding("Name") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
