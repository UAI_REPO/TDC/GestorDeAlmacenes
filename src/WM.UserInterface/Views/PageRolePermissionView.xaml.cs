﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Data;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;
using System;

namespace WM.UserInterface.Views
{
	public class PageRolePermissionView : UserControl
	{
		public PageRolePermissionView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "RolePermission.Id".Translate(), Binding = new Binding("Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "RolePermission.RoleDefinition".Translate(), Binding = new Binding("RoleDefinition.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "RolePermission.Permission".Translate(), Binding = new Binding("Permission.Name") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
