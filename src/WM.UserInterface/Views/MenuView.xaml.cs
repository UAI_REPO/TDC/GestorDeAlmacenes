﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Service.Security;
using Service.Security.Contracts;
using System;
using System.Linq;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;
using WM.UserInterface.Helpers;
using WM.UserInterface.Models;
using WM.UserInterface.Services;
using WM.UserInterface.ViewModels;

namespace WM.UserInterface.Views
{
	public class MenuView : UserControl
	{
		private static ISecurityService securityService { get => SecurityService.Manager; }


		public MenuView()
		{
			this.InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
			InilializeMenuPanel();

			var payload = this.Get<UserControl>("payload");
			payload.Content = new PageHomeViewModel();
		}

		private void InilializeMenuPanel()
		{
			try
			{
				var menuPanel = this.Get<StackPanel>("menuPanel");
				
				var btn = new Button();
				btn.Content = "Menu";
				btn.Classes.Add("menuLabel");
				btn.Bind(Button.CommandProperty, new Binding("OnMenuButtonClick"));

				menuPanel.Children.Add(btn);
				
				var layout = MenuLayout.GetLayout();
				var permissions = securityService.GetUIPermissions();

				foreach (var item in layout.Where(x => permissions.Any(p => p == x.Name)))
				{
					var pItems = item.Items?.Where(x => permissions.Any(p => p == x.Name));

					if (pItems == null || pItems.Count() == 0)
						continue;

					var mi = new UCMenuItem();
					
					mi.SelectedItem += itemClick;
					mi.Items = new UIMenuItem() { Name = item.Name, UIs = pItems.Select(x => x.Name).ToList() };

					menuPanel.Children.Add(mi);
				}

			}
			catch (Exception e)
			{
				var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
				messageBoxStandardWindow.Show();
			}
		}


		private void itemClick(object sender, MenuItemEventArgs e)
		{
			var payload = this.Get<UserControl>("payload");
			payload.Content = UIFactory.GetUI(e.UI);

			e.Handled = true;
		}
	}
}
