﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Data;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;
using System;

namespace WM.UserInterface.Views
{
	public class PageTordHistView : UserControl
	{
		public PageTordHistView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.Id".Translate(), Binding = new Binding("Id") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.TransportOrderId".Translate(), Binding = new Binding("TransportOrderId") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.Tu".Translate(), Binding = new Binding("Tu.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.Status".Translate(), Binding = new Binding("Status") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.Message".Translate(), Binding = new Binding("Message") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.LastLocation".Translate(), Binding = new Binding("LastLocation.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.ActualLocation".Translate(), Binding = new Binding("ActualLocation.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.TargetLocation".Translate(), Binding = new Binding("TargetLocation.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.CreatedBy".Translate(), Binding = new Binding("CreatedBy") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.CreatedAt".Translate(), Binding = new Binding("CreatedAt") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.UpdatedBy".Translate(), Binding = new Binding("UpdatedBy") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportOrderHist.UpdatedAt".Translate(), Binding = new Binding("UpdatedAt") });

		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
