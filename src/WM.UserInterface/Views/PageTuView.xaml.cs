﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Data;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Services.Contracts.Extensions;
using System;

namespace WM.UserInterface.Views
{
	public class PageTuView : UserControl
	{
		public PageTuView()
		{
			this.InitializeComponent();
			InitGrid();
		}

		private void InitGrid()
		{
			var grid = this.Get<DataGrid>("main_grid");
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportUnit.Name".Translate(), Binding = new Binding("Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportUnit.LotId".Translate(), Binding = new Binding("LotId") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportUnit.LocationName".Translate(), Binding = new Binding("Location.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportUnit.MaterialName".Translate(), Binding = new Binding("Material.Name") });
			grid.Columns.Add(new DataGridTextColumn() { Header = "TransportUnit.IsObserved".Translate(), Binding = new Binding("IsObserved") });
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
