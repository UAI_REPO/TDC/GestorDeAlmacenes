﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Services.Contracts;
using Services.Contracts.Extensions;
using Services.Entities.Exceptions;
using System;

namespace WM.UserInterface.Views
{
	public class MainWindow : Window
	{
		public MainWindow()
		{
			ServiceContract.LoadLocalization();

			InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif

			AvaloniaLocator.CurrentMutable.Bind<MainWindow>().ToConstant(this);
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}

		private void OnButtonClick(object sender, RoutedEventArgs e)
		{

			this.Close();
		}

		public void ShowSpinner(string message = null)
		{
			Avalonia.Threading.Dispatcher.UIThread.Post(
								() => { 
									this.Get<TextBlock>("busyDesc").Text = message == null ? "BusyIndicatorMessage".Translate() : message.Translate();
									this.Get<Avalonia.Controls.Primitives.Popup>("busyIndicator").IsOpen = true;
									this.Get<StackPanel>("main_wndow").IsEnabled = false;
								},

								Avalonia.Threading.DispatcherPriority.Normal);
		}

		public void HideSpinner()
		{
			Avalonia.Threading.Dispatcher.UIThread.Post(
								() => 
								{ 
									this.Get<Avalonia.Controls.Primitives.Popup>("busyIndicator").IsOpen = false;
									this.Get<StackPanel>("main_wndow").IsEnabled = true;
								},
								Avalonia.Threading.DispatcherPriority.Normal);
		}
	}
}
