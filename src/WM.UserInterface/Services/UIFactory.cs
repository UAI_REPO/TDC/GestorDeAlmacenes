﻿using Service.Security;
using Service.Security.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;
using WM.UserInterface.ViewModels;

namespace WM.UserInterface.Services
{
	public static class UIFactory
	{

		private static IDictionary<string, Type> _ui;

		private static IDictionary<string, Type> ui 
		{ 
			get 
			{
				if (_ui == null)
					_ui = initializeUIbyRole();

				return _ui;
			} 
			set 
			{
				_ui = value;
			} 
		}

		private static ISecurityService securityService { get => SecurityService.Manager; }

		private static IDictionary<string, Type> initializeUIbyRole()
		{
			var result = new Dictionary<string, Type>()
			{
				{ "UI_Location", typeof(PageLocationViewModel) },
				{ "UI_Topology", typeof(PageTopologyViewModel) },
				{ "UI_TU", typeof(PageTuViewModel) },
				{ "UI_Material", typeof(PageMaterialViewModel) },
				{ "UI_Route", typeof(PageRouteViewModel) },
				{ "UI_GoodsIn", typeof(PageGoodsInViewModel) },
				{ "UI_GoodsOut", typeof(PageGoodsOutViewModel) },
				{ "UI_TransportOrder", typeof(PageTordViewModel) },
				{ "UI_TransportOrderHist", typeof(PageTordHistViewModel) },
				{ "UI_Movements", typeof(PageTordMovementViewModel) },
				{ "UI_MovementsHist", typeof(PageTordMovementHistViewModel) },

				{ "UI_Rule", typeof(PageRuleViewModel) },
				{ "UI_Parameterized", typeof(PageParameterizedViewModel) },

				{ "UI_Permission", typeof(PagePermissionViewModel) },
				{ "UI_Role", typeof(PageRoleViewModel) },
				{ "UI_RoleDefinition", typeof(PageRoleDefinitionViewModel) },
				{ "UI_RolePermission", typeof(PageRolePermissionViewModel) },
				{ "UI_User", typeof(PageUserViewModel) },
				{ "UI_UserPermission", typeof(PageUserPermissionViewModel) },
				{ "UI_UserRole", typeof(PageUserRoleViewModel) },

				{ "UI_Backup", typeof(PageDBHandlerViewModel) },
			};
			var permissions = securityService.GetUIPermissions();
			return result.Where(x => permissions.Any(p => p == x.Key)).ToDictionary(x => x.Key, x => x.Value);
		}

		internal static ViewModelBase GetUI(string key)
		{
			return (ViewModelBase)Activator.CreateInstance(ui[key]);
		}
	}
}
