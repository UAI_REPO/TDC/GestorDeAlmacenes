﻿namespace WM.UserInterface.Models
{
	public class LayoutItem
	{
		public string Name { get; set; }
		public LayoutItem[] Items { get; set; }
	}
}