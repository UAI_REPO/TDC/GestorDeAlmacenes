﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WM.UserInterface.Models
{
	public class DBItem
	{
		public string Path { get; set; }

		public DateTime Date { get; set; }
	}
}
