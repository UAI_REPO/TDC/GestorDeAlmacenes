﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WM.UserInterface.Models
{
	public class UIMenuItem
	{
		public string Name { get; set; }
		public IList<string> UIs { get; set; }
	}
}
