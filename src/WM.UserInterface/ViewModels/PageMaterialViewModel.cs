﻿using Avalonia.Controls.Primitives;
using ReactiveUI;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;

namespace WM.UserInterface.ViewModels
{
	public class PageMaterialViewModel : ViewModelBase
	{
    #region Fields
    string _searchValue; 
    string _searchType;
    Material _selectedItem;
    bool createPopup;
    string errorMessage;
    bool popupOpen;
    #endregion

    #region Properties
    public IMaterialService Service => MaterialService.Manager;

    public ObservableCollection<Material> Items { get; }

    public string SearchValue
    {
      get
      {
        return _searchValue;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchValue, value);
        _searchValue = value;
      }
    }

    public string SearchType
    {
      get
      {
        return _searchType;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchType, value);
        _searchType = value;

        if (string.IsNullOrEmpty(value))
          SearchValue = string.Empty;
      }
    }

    public Material SelectedItem
    {
      get
      {
        return _selectedItem;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _selectedItem, value);
        _selectedItem = value;
      }
    }

    public bool CreatePopup
    {
      get
      {
        return createPopup;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref createPopup, value);
        createPopup = value;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return errorMessage;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref errorMessage, value);
        errorMessage = value;
      }
    }

    public bool PopupOpen
    {
      get
      {
        return popupOpen;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref popupOpen, value);
        popupOpen = value;
      }
    }

    public IList<string> PropertySearchItems => typeof(Material).GetProperties().Select(x => $"{nameof(Material)}.{x.Name}".Translate()).ToList();
    #endregion

    #region Constructor
    public PageMaterialViewModel()
    {
      Items = new ObservableCollection<Material>(Service.GetAllMaterials());
    }
    #endregion

    #region Methods
    public void SearchItems()
    {
      try
      {
        Items.Clear();

        var props = typeof(Material).GetProperties().Select(x => new KeyValuePair<string, PropertyInfo>($"{nameof(Material)}.{x.Name}".Translate(), x)).ToList();
        
        foreach (var item in Service.GetAllMaterials(props.FirstOrDefault(x => x.Key == SearchType).Value.Name, SearchValue))
        {
          Items.Add(item);
        }

      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
      
    }
    
    public void ClearFilters()
    {
      SearchValue = null;
      SearchType = null;
      Items.Clear();

      foreach (var item in Service.GetAllMaterials())
      {
        Items.Add(item);
      }
    }

    public void OnAddClick()
    {
      CreatePopup = true;
      SelectedItem = new Material();
      ErrorMessage = null;
      PopupOpen = true;
    }

    public void OnEditClick()
    {
      CreatePopup = false;
      ErrorMessage = null;
      PopupOpen = true;
    }

    public async Task OnDeleteClick()
    {
      var win = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams
      {
        ContentTitle = "Popup.DeleteMaterialTitle".Translate(),
        ContentMessage = "Popup.DeleteMaterialMessage".Translate(),
        ButtonDefinitions = MessageBox.Avalonia.Enums.ButtonEnum.OkCancel,
        Icon = MessageBox.Avalonia.Enums.Icon.Warning,
        Style = MessageBox.Avalonia.Enums.Style.DarkMode,
        WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner,
        ShowInCenter = true
      });

      var r = await win.Show();

      if (r == MessageBox.Avalonia.Enums.ButtonResult.Ok)
      {
        Service.DeleteMaterial(SelectedItem.Name);
        ClearFilters();
      }
    }

    public void OnConfirm()
    {
      if (CreatePopup)
        CreateNewMaterial();
      else
        EditMaterial();
    }

    public void EditMaterial()
    {
      try
      {
        ErrorMessage = null;

        Service.UpdateMaterial(SelectedItem);

        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;

      }
    }

    public void CreateNewMaterial()
    {
      try
      {
        ErrorMessage = null;

        //Validate Fields
        if (SelectedItem.Name == null)
        {
          ErrorMessage = "Popup.AddMaterialNameValidation".Translate();
          return;
        }

        Service.InsertMaterial(SelectedItem);
        
        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;
      }
    }

    public void ClosePopup()
    {
      PopupOpen = false;
    }
    
    #endregion
  }
}