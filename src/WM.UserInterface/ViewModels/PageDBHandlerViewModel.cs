﻿using Avalonia;
using ReactiveUI;
using Services.Contracts;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Management;
using WM.UserInterface.Models;
using WM.UserInterface.Views;

namespace WM.UserInterface.ViewModels
{
	public class PageDBHandlerViewModel : ViewModelBase
	{
    #region Fields
    DBItem _selectedItem; 
    string errorMessage;
    bool popupOpen;
    string dBName;
    #endregion

    #region Properties

    public ObservableCollection<DBItem> Items { get; set; }

    public DBItem SelectedItem
    {
      get
      {
        return _selectedItem;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _selectedItem, value);
        _selectedItem = value;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return errorMessage;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref errorMessage, value);
        errorMessage = value;
      }
    }


    public string DBName
    {
      get
      {
        return dBName;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref dBName, value);
      }
    }


    public bool PopupOpen
    {
      get
      {
        return popupOpen;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref popupOpen, value);
        popupOpen = value;
      }
    }

    #endregion

    #region Constructor
    public PageDBHandlerViewModel()
    {
      LoadItems();
    }
    #endregion

    #region Methods
    public void LoadItems()
    {
      if (Items != null)
        Items.Clear();
      else
        Items = new ObservableCollection<DBItem>();

      var files = ServiceContract.GetBackupList();

      foreach (FileInfo item in files)
      {
        Items.Add(new DBItem() { Path = item.Name, Date = item.CreationTime });
      }
    }

    public async Task OnRestore(CancellationToken token)
    {
      try
      {
        if (SelectedItem?.Path == null)
        {
          var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", "dbItem.valid.fields".Translate());
          messageBoxStandardWindow.Show();
          return;
        }

        var win = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams
        {
          ContentTitle = "Popup.RestoreBackupTitle".Translate(),
          ContentMessage = "Popup.RestoreBackupMessage".Translate(),
          ButtonDefinitions = MessageBox.Avalonia.Enums.ButtonEnum.OkCancel,
          Icon = MessageBox.Avalonia.Enums.Icon.Warning,
          Style = MessageBox.Avalonia.Enums.Style.DarkMode,
          WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner,
          ShowInCenter = true
        });

        var r = await win.Show();

        if (r == MessageBox.Avalonia.Enums.ButtonResult.Ok)
        {
          this.ShowSpinner();
          Task.Run(() =>
          {
						try
						{
              ServiceContract.RestoreDB(SelectedItem.Path);

              Avalonia.Threading.Dispatcher.UIThread.Post(
                () => {
                  var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Info", "data.restore.success".Translate());
                  messageBoxStandardWindow.Show();
                },
                Avalonia.Threading.DispatcherPriority.Normal);
            }
            catch (Exception e)
            {
              Avalonia.Threading.Dispatcher.UIThread.Post(
                () => {
                  var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
                  messageBoxStandardWindow.Show();
                },
                Avalonia.Threading.DispatcherPriority.Normal);
            }
            finally
            {
              this.HideSpinner();
            }

          });
          
        }
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    public async Task OnBackup(CancellationToken token)
    {
      SelectedItem = new DBItem();
      ErrorMessage = null;
      PopupOpen = true;
    }

    
    public void OnConfirm()
    {
      try
      {
        if (DBName == null)
        {
          ErrorMessage = "dbItem.valid.fields".Translate();
          return;
        }

        ErrorMessage = null;

        if (!DBName.EndsWith(".bak"))
          DBName += ".bak";

        ServiceContract.BackupDB(DBName);
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Info", "data.backup.success".Translate());
        messageBoxStandardWindow.Show();

        PopupOpen = false;
        LoadItems();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;
      }
    }

    public void ClosePopup()
    {
      PopupOpen = false;
    }


    public async Task OnDelete(CancellationToken token)
    {
      var win = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams
      {
        ContentTitle = "Popup.DeleteBackupTitle".Translate(),
        ContentMessage = "Popup.DeleteBackupMessage".Translate(),
        ButtonDefinitions = MessageBox.Avalonia.Enums.ButtonEnum.OkCancel,
        Icon = MessageBox.Avalonia.Enums.Icon.Warning,
        Style = MessageBox.Avalonia.Enums.Style.DarkMode,
        WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner,
        ShowInCenter = true
      });

      var r = await win.Show();

      if (r == MessageBox.Avalonia.Enums.ButtonResult.Ok)
      {
        ServiceContract.DeleteBackup(SelectedItem.Path);

        LoadItems();
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Info", "data.delete.success".Translate());
        messageBoxStandardWindow.Show();
      }
    }

    #endregion
  }
}