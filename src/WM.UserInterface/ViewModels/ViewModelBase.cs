﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Avalonia;
using ReactiveUI;
using WM.UserInterface.Views;

namespace WM.UserInterface.ViewModels
{
	public class ViewModelBase : ReactiveObject
	{
		public void ShowSpinner(string message = null)
		{
			var main = AvaloniaLocator.Current.GetService<MainWindow>();
			main.ShowSpinner(message);
		}

		public void HideSpinner()
		{
			var main = AvaloniaLocator.Current.GetService<MainWindow>();
			main.HideSpinner();
		}
	}
}
