﻿using Avalonia.Controls.Primitives;
using ReactiveUI;
using Service.Security;
using Service.Security.Contracts;
using Service.Security.Entites.DB;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;

namespace WM.UserInterface.ViewModels
{
	public class PageRoleViewModel : ViewModelBase
	{
    #region Fields
    string _searchValue; 
    string _searchType;
    Role _selectedItem;
    bool createPopup;
    string errorMessage;
    bool popupOpen;
    #endregion

    #region Properties
    public ISecurityDataService Service => SecurityDataService.Manager;

    public ObservableCollection<Role> Items { get; }

    public string SearchValue
    {
      get
      {
        return _searchValue;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchValue, value);
        _searchValue = value;
      }
    }

    public string SearchType
    {
      get
      {
        return _searchType;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchType, value);
        _searchType = value;

        if (string.IsNullOrEmpty(value))
          SearchValue = string.Empty;
      }
    }

    public Role SelectedItem
    {
      get
      {
        return _selectedItem;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _selectedItem, value);
        _selectedItem = value;
      }
    }

    public bool CreatePopup
    {
      get
      {
        return createPopup;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref createPopup, value);
        createPopup = value;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return errorMessage;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref errorMessage, value);
        errorMessage = value;
      }
    }

    public bool PopupOpen
    {
      get
      {
        return popupOpen;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref popupOpen, value);
        popupOpen = value;
      }
    }

    public IList<string> PropertySearchItems => typeof(Role).GetProperties().Select(x => $"{nameof(Role)}.{x.Name}".Translate()).ToList();
    #endregion

    #region Constructor
    public PageRoleViewModel()
    {
      Items = new ObservableCollection<Role>(Service.GetAllRoles());
    }
    #endregion

    #region Methods
    public void SearchItems()
    {
      try
      {
        Items.Clear();

        var props = typeof(Role).GetProperties().Select(x => new KeyValuePair<string, PropertyInfo>($"{nameof(Role)}.{x.Name}".Translate(), x)).ToList();

        var prop = props.FirstOrDefault(x => x.Key == SearchType);

        var field = prop.Value.Name;
        var type = prop.Value.PropertyType.Namespace == typeof(Role).Namespace ? prop.Value.PropertyType : null;

        if (type != null)
          field = type.GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name;

        foreach (var item in Service.GetAllRoles(field, SearchValue, type))
        {
          Items.Add(item);
        }

      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
      
    }
    
    public void ClearFilters()
    {
      SearchValue = null;
      SearchType = null;
      Items.Clear();

      foreach (var item in Service.GetAllRoles())
      {
        Items.Add(item);
      }
    }

    public void OnAddClick()
    {
      CreatePopup = true;
      SelectedItem = new Role();
      ErrorMessage = null;
      PopupOpen = true;
    }

    public void OnEditClick()
    {
      CreatePopup = false;
      ErrorMessage = null;
      PopupOpen = true;
    }

    public async Task OnDeleteClick()
    {
      var win = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams
      {
        ContentTitle = "Popup.DeleteRoleTitle".Translate(),
        ContentMessage = "Popup.DeleteRoleMessage".Translate(),
        ButtonDefinitions = MessageBox.Avalonia.Enums.ButtonEnum.OkCancel,
        Icon = MessageBox.Avalonia.Enums.Icon.Warning,
        Style = MessageBox.Avalonia.Enums.Style.DarkMode,
        WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner,
        ShowInCenter = true
      });

      var r = await win.Show();

      if (r == MessageBox.Avalonia.Enums.ButtonResult.Ok)
      {
        Service.DeleteRole(SelectedItem.Id);
        ClearFilters();
      }
    }

    public void OnConfirm()
    {
      if (CreatePopup)
        CreateNewRole();
      else
        EditRole();
    }

    public void EditRole()
    {
      try
      {
        ErrorMessage = null;

        Service.UpdateRole(SelectedItem);

        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;

      }
    }

    public void CreateNewRole()
    {
      try
      {
        ErrorMessage = null;


        Service.InsertRole(SelectedItem);
        
        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;
      }
    }

    public void ClosePopup()
    {
      PopupOpen = false;
    }
    
    #endregion
  }
}