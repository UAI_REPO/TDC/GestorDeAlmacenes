﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using Services.Contracts.Extensions;
using System.IO;
using Avalonia;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;
using Services.Contracts;
using Service.Security;
using Service.Security.Contracts;
using Service.Security.Entites.DB;
using System.Threading.Tasks;

namespace WM.UserInterface.ViewModels
{
	public class MainWindowViewModel : ViewModelBase
	{
		#region Fields
		bool isUserLogger;
		string userName;
		string userPassword;
		string userLanguage;
		IList<string> userLanguageList;
		string message;
		ViewModelBase menuContent;
		#endregion

		#region Properties

		public bool IsUserLogger
		{
			get
			{
				return isUserLogger;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref isUserLogger, value);
				isUserLogger = value;
			}
		}
		public string UserName
		{
			get
			{
				return userName;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref userName, value);
				userName = value;
			}
		}
		public string UserPassword
		{
			get
			{
				return userPassword;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref userPassword, value);
				userPassword = value;
			}
		}
		public string UserLanguage
		{
			get
			{
				return userLanguage;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref userLanguage, value);
				userLanguage = value;
			}
		}
		public string Message
		{
			get
			{
				return message;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref message, value);
				message = value;
			}
		}
		public ViewModelBase MenuContent
		{
			get
			{
				return menuContent;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref menuContent, value);
				menuContent = value;
			}
		}

		public IList<string> UserLanguageList
		{
			get
			{
				return userLanguageList;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref userLanguageList, value);
				userLanguageList = value;
			}
		}

		public ISecurityService Security => SecurityService.Manager;
		#endregion

		#region Constructor
		public MainWindowViewModel()
		{
			LoadRegionalization();
			
			UserLanguage = UserLanguageList[1];

			this.ShowSpinner("Initializing.DB");
			Task.Run(() =>
			{
				try
				{
					ServiceContract.CheckOrInitializeDB();
				}
				catch (Exception e)
				{
					"Error initializing the database".LogError(e);
					var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
					messageBoxStandardWindow.Show();
				}
				finally
				{
					this.HideSpinner();
				}

			});
		}

		#endregion

		#region Methods
		public void LoadRegionalization()
		{
			try
			{
				var regioList = new List<string>();
				var path = "./I18n/";
				foreach (string dir in Directory.GetFiles(path))
				{
					regioList.Add(dir.Substring(path.Length, dir.IndexOf(".json") - path.Length));
				}

				UserLanguageList = regioList;
			}
			catch (Exception e)
			{
				"Error getting the config data".LogError(e);
				var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
				messageBoxStandardWindow.Show();
			}

		}

		public void OnLogOffClick()
		{
			try
			{

				//Load Menuview
				MenuContent = null;

				IsUserLogger = false;
			}
			catch (Exception e)
			{
				"Error login off the user".LogError(e);
			}
		}

		public void OnLoginClick()
		{
			try
			{
				Message = string.Empty;

				if (!SecurityService.Manager.CheckDV())
				{
					Message = "LoginPopup_Wrong_DV".Translate();
					return;
				}

				if (string.IsNullOrEmpty(UserName))
				{
					Message = "LoginPopup_UserNameEmpty".Translate();
					return;
				}

				if (string.IsNullOrEmpty(UserPassword))
				{
					Message = "LoginPopup_UserPasswordEmpty".Translate();
					return;
				}

				if (string.IsNullOrEmpty(UserLanguage))
				{
					Message = "LoginPopup_UserLanguageEmpty".Translate();
					return;
				}

				User user = Security.GetUser(UserName, UserPassword);

				Security.SetCurrentUser(user);

				ServiceContract.LoadLocalization(UserLanguage);

				//Load Menuview
				MenuContent = new MenuViewModel();

				IsUserLogger = true;
			}
			catch (Exception e)
			{
				"Error login the user".LogError(e);
				Message = e.Message;
			}
		}

		#endregion
	}
}
