﻿using Avalonia.Controls.Primitives;
using ReactiveUI;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;
using WM.ProcessingLayer.Management.SearchRules;

namespace WM.UserInterface.ViewModels
{
  public class PageRuleViewModel : ViewModelBase
  {
    #region Fields
    string _searchValue;
    string _searchType;
    Rule _selectedItem;
    bool createPopup;
    string errorMessage;
    bool popupOpen;
    #endregion

    #region Properties
    public IRulesManagement Service => RulesManagement.Manager;

    public ObservableCollection<Rule> Items { get; set; }

    public string SearchValue
    {
      get
      {
        return _searchValue;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchValue, value);
        _searchValue = value;
      }
    }

    public string SearchType
    {
      get
      {
        return _searchType;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchType, value);
        _searchType = value;

        if (string.IsNullOrEmpty(value))
          SearchValue = string.Empty;
      }
    }

    public Rule SelectedItem
    {
      get
      {
        return _selectedItem;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _selectedItem, value);
        _selectedItem = value;
      }
    }

    public bool CreatePopup
    {
      get
      {
        return createPopup;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref createPopup, value);
        createPopup = value;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return errorMessage;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref errorMessage, value);
        errorMessage = value;
      }
    }

    public bool PopupOpen
    {
      get
      {
        return popupOpen;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref popupOpen, value);
        popupOpen = value;
      }
    }

    public IList<string> PropertySearchItems => typeof(Rule).GetProperties().Select(x => $"{nameof(Rule)}.{x.Name}".Translate()).ToList();

    public IList<LocationType> LocationTypes { get; } = Enum.GetValues(typeof(LocationType)).Cast<LocationType>().ToList();

    public IEnumerable<string> RuleNames => new string[] { nameof(FreeLocationRule), nameof(ParameterizedRule) };
    public ObservableCollection<Material> Materials { get; set; }

    #endregion

    #region Constructor
    public PageRuleViewModel()
    {
      Items = new ObservableCollection<Rule>(Service.GetAllRules());

      LoadPopupItems();
    }
    #endregion

    #region Methods
    public void LoadPopupItems()
    {
      Materials = new ObservableCollection<Material>(MaterialService.Manager.GetAllMaterials());
    }
    public void SearchItems()
    {
      try
      {
        Items.Clear();

        var props = typeof(Rule).GetProperties().Select(x => new KeyValuePair<string, PropertyInfo>($"{nameof(Rule)}.{x.Name}".Translate(), x)).ToList();

        var prop = props.FirstOrDefault(x => x.Key == SearchType);

        var field = prop.Value.Name;
        var type = prop.Value.PropertyType.Namespace == typeof(Rule).Namespace ? prop.Value.PropertyType : null;

        if (type != null)
          field = type.GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name;

        foreach (var item in Service.GetAllRules(field, SearchValue, type))
        {
          Items.Add(item);
        }

      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
      
    }
    
    public void ClearFilters()
    {
      SearchValue = null;
      SearchType = null;
      Items.Clear();

      foreach (var item in Service.GetAllRules())
      {
        Items.Add(item);
      }
    }

    public void OnAddClick()
    {
      LoadPopupItems();
      CreatePopup = true;
      SelectedItem = new Rule();
      ErrorMessage = null;
      PopupOpen = true;
    }

    public void OnEditClick()
    {
      LoadPopupItems();
      CreatePopup = false;
      ErrorMessage = null;
      PopupOpen = true;
    }

    public async Task OnDeleteClick()
    {
      var win = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams
      {
        ContentTitle = "Popup.DeleteMaterialTitle".Translate(),
        ContentMessage = "Popup.DeleteMaterialMessage".Translate(),
        ButtonDefinitions = MessageBox.Avalonia.Enums.ButtonEnum.OkCancel,
        Icon = MessageBox.Avalonia.Enums.Icon.Warning,
        Style = MessageBox.Avalonia.Enums.Style.DarkMode,
        WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner,
        ShowInCenter = true
      });

      var r = await win.Show();

      if (r == MessageBox.Avalonia.Enums.ButtonResult.Ok)
      {
        Service.DeleteRule(SelectedItem.Id);
        ClearFilters();
      }
    }

    public void OnConfirm()
    {
      if (CreatePopup)
        CreateNewRule();
      else
        EditRule();
    }

    public void EditRule()
    {
      try
      {
        ErrorMessage = null;

        Service.UpdateRule(SelectedItem);

        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;

      }
    }

    public void CreateNewRule()
    {
      try
      {
        ErrorMessage = null;

        Service.InsertRule(SelectedItem);
        
        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;
      }
    }

    public void ClosePopup()
    {
      PopupOpen = false;
    }
    
    #endregion
  }
}