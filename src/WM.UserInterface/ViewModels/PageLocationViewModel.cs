﻿using Avalonia.Controls.Primitives;
using ReactiveUI;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;

namespace WM.UserInterface.ViewModels
{
	public class PageLocationViewModel : ViewModelBase
	{
		#region Fields
		string _searchValue;
		string _searchType;
		Location _selectedItem;
		bool createPopup;
		string errorMessage;
		bool popupOpen;
		#endregion

		#region Properties
		public ITopologyService Service => TopologyService.Manager;

		public ObservableCollection<Location> Locations { get; }

		public string SearchValue
		{
			get
			{
				return _searchValue;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref _searchValue, value);
				_searchValue = value;
			}
		}

		public string SearchType
		{
			get
			{
				return _searchType;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref _searchType, value);
				_searchType = value;

				if (string.IsNullOrEmpty(value))
					SearchValue = string.Empty;
			}
		}

		public IList<string> PropertySearchItems => typeof(Location).GetProperties().Select(x => $"{nameof(Location)}.{x.Name}".Translate()).ToList();

		public Location SelectedItem
		{
			get
			{
				return _selectedItem;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref _selectedItem, value);
				_selectedItem = value;
			}
		}

		public bool CreatePopup
		{
			get
			{
				return createPopup;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref createPopup, value);
				createPopup = value;
			}
		}

		public string ErrorMessage
		{
			get
			{
				return errorMessage;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref errorMessage, value);
				errorMessage = value;
			}
		}

		public bool PopupOpen
		{
			get
			{
				return popupOpen;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref popupOpen, value);
				popupOpen = value;
			}
		}

		public IList<LocationType> LocationTypes { get; } = Enum.GetValues(typeof(LocationType)).Cast<LocationType>().ToList();
		#endregion

		#region Constructor
		public PageLocationViewModel()
		{
			Locations = new ObservableCollection<Location>(Service.GetAllLocations());
		}
		#endregion

		#region Methods
		public void SearchItems()
		{
			try
			{
				Locations.Clear();

				var props = typeof(Location).GetProperties().Select(x => new KeyValuePair<string, PropertyInfo>($"{nameof(Location)}.{x.Name}".Translate(), x)).ToList();

				foreach (var item in Service.GetAllLocations(props.FirstOrDefault(x => x.Key == SearchType).Value.Name, SearchValue))
				{
					Locations.Add(item);
				}

			}
			catch (Exception e)
			{
				var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
				messageBoxStandardWindow.Show();
			}

		}
		public void ClearFilters()
		{
			SearchValue = null;
			SearchType = null;
			Locations.Clear();

			foreach (var item in Service.GetAllLocations())
			{
				Locations.Add(item);
			}
		}

		public void OnAddClick()
		{
			CreatePopup = true;
			SelectedItem = new Location();
			ErrorMessage = null;
			PopupOpen = true;
		}

		public void OnEditClick()
		{
			CreatePopup = false;
			ErrorMessage = null;
			PopupOpen = true;
		}

		public async Task OnDeleteClick()
		{
			var win = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams
			{
				ContentTitle = "Popup.DeleteLocationTitle".Translate(),
				ContentMessage = "Popup.DeleteLocationMessage".Translate(),
				ButtonDefinitions = MessageBox.Avalonia.Enums.ButtonEnum.OkCancel,
				Icon = MessageBox.Avalonia.Enums.Icon.Warning,
				Style = MessageBox.Avalonia.Enums.Style.DarkMode,
				WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner,
				ShowInCenter = true
			});

			var r = await win.Show();

			if (r == MessageBox.Avalonia.Enums.ButtonResult.Ok)
			{
				try
				{
					TopologyService.Manager.DeleteLocation(SelectedItem.Name);
					ClearFilters();
				}
				catch (Exception e)
				{
					var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
					messageBoxStandardWindow.Show();
				}

			}
		}

		public void OnConfirm()
		{
			if (CreatePopup)
				CreateNewLocation();
			else
				EditLocation();
		}

		public void EditLocation()
		{
			try
			{
				ErrorMessage = null;


				if (SelectedItem.Parent == string.Empty) SelectedItem.Parent = null;

				TopologyService.Manager.UpdateLocation(SelectedItem);

				PopupOpen = false;
				ClearFilters();
			}
			catch (Exception e)
			{
				ErrorMessage = e.Message;

			}
		}

		public void CreateNewLocation()
		{
			try
			{
				ErrorMessage = null;

				//Validate Fields
				if (SelectedItem.Name == null)
				{
					ErrorMessage = "Popup.AddLocationNameValidation".Translate();
					return;
				}

				TopologyService.Manager.InsertLocation(SelectedItem);

				PopupOpen = false;
				ClearFilters();
			}
			catch (Exception e)
			{
				ErrorMessage = e.Message;

			}
		}

		public void ClosePopup()
		{
			PopupOpen = false;
		}

		#endregion
	}
}