﻿using Avalonia.Controls.Primitives;
using ReactiveUI;
using Service.Security;
using Service.Security.Contracts;
using Service.Security.Entites.DB;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;

namespace WM.UserInterface.ViewModels
{
	public class PageUserViewModel : ViewModelBase
	{
    #region Fields
    string _searchValue; 
    string _searchType;
    User _selectedItem;
    bool createPopup;
    string errorMessage;
    bool popupOpen;
    #endregion

    #region Properties
    public ISecurityDataService Service => SecurityDataService.Manager;

    public ObservableCollection<User> Items { get; }

    public string SearchValue
    {
      get
      {
        return _searchValue;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchValue, value);
        _searchValue = value;
      }
    }

    public string SearchType
    {
      get
      {
        return _searchType;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchType, value);
        _searchType = value;

        if (string.IsNullOrEmpty(value))
          SearchValue = string.Empty;
      }
    }

    public User SelectedItem
    {
      get
      {
        return _selectedItem;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _selectedItem, value);
        _selectedItem = value;
      }
    }

    public bool CreatePopup
    {
      get
      {
        return createPopup;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref createPopup, value);
        createPopup = value;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return errorMessage;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref errorMessage, value);
        errorMessage = value;
      }
    }

    public bool PopupOpen
    {
      get
      {
        return popupOpen;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref popupOpen, value);
        popupOpen = value;
      }
    }

    public IList<string> PropertySearchItems => typeof(User).GetProperties().Select(x => $"{nameof(User)}.{x.Name}".Translate()).ToList();
    #endregion

    #region Constructor
    public PageUserViewModel()
    {
      Items = new ObservableCollection<User>(Service.GetAllUsers());
    }
    #endregion

    #region Methods
    public void SearchItems()
    {
      try
      {
        Items.Clear();

        var props = typeof(User).GetProperties().Select(x => new KeyValuePair<string, PropertyInfo>($"{nameof(User)}.{x.Name}".Translate(), x)).ToList();

        var prop = props.FirstOrDefault(x => x.Key == SearchType);

        var field = prop.Value.Name;
        var type = prop.Value.PropertyType.Namespace == typeof(User).Namespace ? prop.Value.PropertyType : null;

        if (type != null)
          field = type.GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name;

        foreach (var item in Service.GetAllUsers(field, SearchValue, type))
        {
          Items.Add(item);
        }

      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
      
    }
    
    public void ClearFilters()
    {
      SearchValue = null;
      SearchType = null;
      Items.Clear();

      foreach (var item in Service.GetAllUsers())
      {
        Items.Add(item);
      }
    }

    public void OnAddClick()
    {
      CreatePopup = true;
      SelectedItem = new User();
      ErrorMessage = null;
      PopupOpen = true;
    }

    public void OnEditClick()
    {
      CreatePopup = false;
      ErrorMessage = null;
      PopupOpen = true;
    }

    public async Task OnDeleteClick()
    {
      var win = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams
      {
        ContentTitle = "Popup.DeleteUserTitle".Translate(),
        ContentMessage = "Popup.DeleteUserMessage".Translate(),
        ButtonDefinitions = MessageBox.Avalonia.Enums.ButtonEnum.OkCancel,
        Icon = MessageBox.Avalonia.Enums.Icon.Warning,
        Style = MessageBox.Avalonia.Enums.Style.DarkMode,
        WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner,
        ShowInCenter = true
      });

      var r = await win.Show();

      if (r == MessageBox.Avalonia.Enums.ButtonResult.Ok)
      {
        Service.DeleteUser(SelectedItem.Id);
        ClearFilters();
      }
    }

    public void OnConfirm()
    {
      if (CreatePopup)
        CreateNewItem();
      else
        EditItem();
    }

    public void EditItem()
    {
      try
      {
        ErrorMessage = null;

        Service.UpdateUser(SelectedItem);

        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;

      }
    }

    public void CreateNewItem()
    {
      try
      {
        ErrorMessage = null;

        Service.InsertUser(SelectedItem);
        
        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;
      }
    }

    public void ClosePopup()
    {
      PopupOpen = false;
    }
    
    #endregion
  }
}