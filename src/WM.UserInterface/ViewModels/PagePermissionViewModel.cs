﻿using Avalonia.Controls.Primitives;
using ReactiveUI;
using Service.Security;
using Service.Security.Contracts;
using Service.Security.Entites.DB;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;

namespace WM.UserInterface.ViewModels
{
	public class PagePermissionViewModel : ViewModelBase
	{
    #region Fields
    string _searchValue; 
    string _searchType;
    Permission _selectedItem;
    bool createPopup;
    string errorMessage;
    bool popupOpen;
    #endregion

    #region Properties
    public ISecurityDataService Service => SecurityDataService.Manager;

    public ObservableCollection<Permission> Items { get; }

    public string SearchValue
    {
      get
      {
        return _searchValue;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchValue, value);
        _searchValue = value;
      }
    }

    public string SearchType
    {
      get
      {
        return _searchType;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchType, value);
        _searchType = value;

        if (string.IsNullOrEmpty(value))
          SearchValue = string.Empty;
      }
    }

    public Permission SelectedItem
    {
      get
      {
        return _selectedItem;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _selectedItem, value);
        _selectedItem = value;
      }
    }

    public bool CreatePopup
    {
      get
      {
        return createPopup;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref createPopup, value);
        createPopup = value;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return errorMessage;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref errorMessage, value);
        errorMessage = value;
      }
    }

    public bool PopupOpen
    {
      get
      {
        return popupOpen;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref popupOpen, value);
        popupOpen = value;
      }
    }

    public IList<string> PropertySearchItems => typeof(Permission).GetProperties().Select(x => $"{nameof(Permission)}.{x.Name}".Translate()).ToList();
    #endregion

    #region Constructor
    public PagePermissionViewModel()
    {
      Items = new ObservableCollection<Permission>(Service.GetAllPermission());
    }
    #endregion

    #region Methods
    public void SearchItems()
    {
      try
      {
        Items.Clear();

        var props = typeof(Permission).GetProperties().Select(x => new KeyValuePair<string, PropertyInfo>($"{nameof(Permission)}.{x.Name}".Translate(), x)).ToList();

        var prop = props.FirstOrDefault(x => x.Key == SearchType);

        var field = prop.Value.Name;
        var type = prop.Value.PropertyType.Namespace == typeof(Permission).Namespace ? prop.Value.PropertyType : null;

        if (type != null)
          field = type.GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name;

        foreach (var item in Service.GetAllPermission(field, SearchValue, type))
        {
          Items.Add(item);
        }

      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
      
    }
    
    public void ClearFilters()
    {
      SearchValue = null;
      SearchType = null;
      Items.Clear();

      foreach (var item in Service.GetAllPermission())
      {
        Items.Add(item);
      }
    }

    public void OnAddClick()
    {
      CreatePopup = true;
      SelectedItem = new Permission();
      ErrorMessage = null;
      PopupOpen = true;
    }

    public void OnEditClick()
    {
      CreatePopup = false;
      ErrorMessage = null;
      PopupOpen = true;
    }

    public async Task OnDeleteClick()
    {
      var win = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams
      {
        ContentTitle = "Popup.DeletePermissionTitle".Translate(),
        ContentMessage = "Popup.DeletePermissionMessage".Translate(),
        ButtonDefinitions = MessageBox.Avalonia.Enums.ButtonEnum.OkCancel,
        Icon = MessageBox.Avalonia.Enums.Icon.Warning,
        Style = MessageBox.Avalonia.Enums.Style.DarkMode,
        WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner,
        ShowInCenter = true
      });

      var r = await win.Show();

      if (r == MessageBox.Avalonia.Enums.ButtonResult.Ok)
      {
        Service.DeletePermission(SelectedItem.Id);
        ClearFilters();
      }
    }

    public void OnConfirm()
    {
      if (CreatePopup)
        CreateNewPermission();
      else
        EditPermission();
    }

    public void EditPermission()
    {
      try
      {
        ErrorMessage = null;

        Service.UpdatePermission(SelectedItem);

        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;

      }
    }

    public void CreateNewPermission()
    {
      try
      {
        ErrorMessage = null;

        //Validate Fields
        if (SelectedItem.Name == null)
        {
          ErrorMessage = "Popup.AddPermissionNameValidation".Translate();
          return;
        }

        Service.InsertPermission(SelectedItem);
        
        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;
      }
    }

    public void ClosePopup()
    {
      PopupOpen = false;
    }
    
    #endregion
  }
}