﻿using Avalonia.Controls.Primitives;
using ReactiveUI;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;

namespace WM.UserInterface.ViewModels
{
	public class PageTordMovementHistViewModel : ViewModelBase
	{
    #region Fields
    string _searchValue; 
    string _searchType;
    TordMovementHist _selectedItem;
    #endregion

    #region Properties
    public ITransportMovementService Service => TransportMovementService.Manager;

    public ObservableCollection<TordMovementHist> Items { get; }

    public string SearchValue
    {
      get
      {
        return _searchValue;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchValue, value);
        _searchValue = value;
      }
    }

    public string SearchType
    {
      get
      {
        return _searchType;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchType, value);
        _searchType = value;

        if (string.IsNullOrEmpty(value))
          SearchValue = string.Empty;
      }
    }

    public IList<string> PropertySearchItems => typeof(TordMovementHist).GetProperties().Select(x => $"{nameof(TordMovementHist)}.{x.Name}".Translate()).ToList();

    public TordMovementHist SelectedItem
    {
      get
      {
        return _selectedItem;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _selectedItem, value);
        _selectedItem = value;
      }
    }

    #endregion

    #region Constructor
    public PageTordMovementHistViewModel()
    {
      Items = new ObservableCollection<TordMovementHist>(Service.GetAllMovementsHist());
    }
    #endregion

    #region Methods
    public void SearchItems()
    {
      try
      {
        Items.Clear();

        var props = typeof(TordMovementHist).GetProperties().Select(x => new KeyValuePair<string, PropertyInfo>($"{nameof(TordMovementHist)}.{x.Name}".Translate(), x)).ToList();

        var prop = props.FirstOrDefault(x => x.Key == SearchType);

        var field = prop.Value.Name;
        var type = prop.Value.PropertyType.Namespace == typeof(TordMovementHist).Namespace ? prop.Value.PropertyType : null;

        if (type != null)
          field = type.GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name;

        foreach (var item in Service.GetAllMovementsHist(field, SearchValue, type))
        {
          Items.Add(item);
        }

      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
      
    }
    public void ClearFilters()
    {
      SearchValue = null;
      SearchType = null;
      Items.Clear();

      foreach (var item in Service.GetAllMovementsHist())
      {
        Items.Add(item);
      }
    }
    #endregion
  }
}