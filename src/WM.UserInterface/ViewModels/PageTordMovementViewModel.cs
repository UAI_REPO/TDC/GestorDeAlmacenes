﻿using Avalonia;
using ReactiveUI;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Management;
using WM.UserInterface.Views;

namespace WM.UserInterface.ViewModels
{
	public class PageTordMovementViewModel : ViewModelBase
	{
    #region Fields
    string _searchValue; 
    string _searchType;
    TordMovement _selectedItem;
    Location _targetLocation;
    string errorMessage;
    bool popupOpen;
    #endregion

    #region Properties
    public ITransportMovementService Service => TransportMovementService.Manager;

    public ObservableCollection<TordMovement> Items { get; }

    public string SearchValue
    {
      get
      {
        return _searchValue;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchValue, value);
        _searchValue = value;
      }
    }

    public string SearchType
    {
      get
      {
        return _searchType;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchType, value);
        _searchType = value;

        if (string.IsNullOrEmpty(value))
          SearchValue = string.Empty;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return errorMessage;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref errorMessage, value);
        errorMessage = value;
      }
    }

    public bool PopupOpen
    {
      get
      {
        return popupOpen;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref popupOpen, value);
        popupOpen = value;
      }
    }

    public IList<string> PropertySearchItems => typeof(TordMovement).GetProperties().Select(x => $"{nameof(TordMovement)}.{x.Name}".Translate()).ToList();

    public TordMovement SelectedItem
    {
      get
      {
        return _selectedItem;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _selectedItem, value);
        _selectedItem = value;
      }
    }

    public Location TargetLocation
    {
      get
      {
        return _targetLocation;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _targetLocation, value);
        _targetLocation = value;
      }
    }

    public ObservableCollection<Location> Locations { get; set; }
    #endregion

    #region Constructor
    public PageTordMovementViewModel()
    {
      Items = new ObservableCollection<TordMovement>(Service.GetAllMovements());
      LoadPopupItems();
    }
    #endregion

    #region Methods
    public void LoadPopupItems()
    {
      Locations = new ObservableCollection<Location>(TopologyService.Manager.GetAllLocations());
    }

    public void SearchItems()
    {
      try
      {
        Items.Clear();

        var props = typeof(TordMovement).GetProperties().Select(x => new KeyValuePair<string, PropertyInfo>($"{nameof(TordMovement)}.{x.Name}".Translate(), x)).ToList();

        var prop = props.FirstOrDefault(x => x.Key == SearchType);

        var field = prop.Value.Name;
        var type = prop.Value.PropertyType.Namespace == typeof(TordMovement).Namespace ? prop.Value.PropertyType : null;

        if (type != null)
          field = type.GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name;

        foreach (var item in Service.GetAllMovements(field, SearchValue, type))
        {
          Items.Add(item);
        }

      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
      
    }
    public void ClearFilters()
    {
      SearchValue = null;
      SearchType = null;
      Items.Clear();

      foreach (var item in Service.GetAllMovements())
      {
        Items.Add(item);
      }
    }

    public async Task OnExport(CancellationToken token)
    {
      try
      {
        var dialog = new Avalonia.Controls.SaveFileDialog();
        dialog.Filters.Add(new Avalonia.Controls.FileDialogFilter() { Name ="pdf", Extensions = { "pdf" } });

        var main = AvaloniaLocator.Current.GetService<MainWindow>();
        var path = await dialog.ShowAsync(main);

        Service.GenerateMovementsReport(path);

        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Info", "exportdatasuccess".Translate());
        messageBoxStandardWindow.Show();
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }

    }

    public void OnPickUp()
    {
      try
      {
        Service.Pickup(SelectedItem.Tu.Name);
        ClearFilters();
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }
    public void OnDeposit()
    {
      LoadPopupItems();
      ErrorMessage = null;
      PopupOpen = true;
    }

    public void OnCancel()
    {
      try
      {

        ClearFilters();
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    public void OnConfirm()
    {
      try
      {
        if (TargetLocation == null)
        {
          ErrorMessage = "tord.movement.valid.fields".Translate();
          return;
        }

        ErrorMessage = null;

        Service.Deposit(SelectedItem.Tu.Name, TargetLocation.Name);

        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;
      }
    }

    public void ClosePopup()
    {
      PopupOpen = false;
    }

    public async Task OnSerialize(CancellationToken token)
    {
      try
      {
        var dialog = new Avalonia.Controls.SaveFileDialog();
        dialog.Filters.Add(new Avalonia.Controls.FileDialogFilter() { Name = "json", Extensions = { "json" } });

        var main = AvaloniaLocator.Current.GetService<MainWindow>();
        var path = await dialog.ShowAsync(main);

        Service.SerializeMovements(path);

        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Info", "exportdatasuccess".Translate());
        messageBoxStandardWindow.Show();
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }
    #endregion
  }
}