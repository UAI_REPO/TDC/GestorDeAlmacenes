﻿using Avalonia.Controls.Primitives;
using ReactiveUI;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;

namespace WM.UserInterface.ViewModels
{
	public class PageTordViewModel : ViewModelBase
	{
    #region Fields
    string _searchValue; 
    string _searchType;
    TransportOrder _selectedItem;
    TransportUnit _targetTu;
    Location _targetLocation;
    string errorMessage;
    bool popupOpen;
    #endregion

    #region Properties
    public ITransportControl Service => TransportControl.Manager;

    public ObservableCollection<TransportOrder> Items { get; }

    public string SearchValue
    {
      get
      {
        return _searchValue;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchValue, value);
        _searchValue = value;
      }
    }

    public string SearchType
    {
      get
      {
        return _searchType;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchType, value);
        _searchType = value;

        if (string.IsNullOrEmpty(value))
          SearchValue = string.Empty;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return errorMessage;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref errorMessage, value);
        errorMessage = value;
      }
    }

    public bool PopupOpen
    {
      get
      {
        return popupOpen;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref popupOpen, value);
        popupOpen = value;
      }
    }

    public IList<string> PropertySearchItems => typeof(TransportOrder).GetProperties().Select(x => $"{nameof(TransportOrder)}.{x.Name}".Translate()).ToList();

    public TransportOrder SelectedItem
    {
      get
      {
        return _selectedItem;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _selectedItem, value);
        _selectedItem = value;
      }
    }

    public TransportUnit TargetTu
    {
      get
      {
        return _targetTu;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _targetTu, value);
        _targetTu = value;
      }
    }

    public Location TargetLocation
    {
      get
      {
        return _targetLocation;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _targetLocation, value);
        _targetLocation = value;
      }
    }

    public ObservableCollection<Location> Locations { get; set; }
    public ObservableCollection<TransportUnit> TransportUnits { get; set; }
    #endregion

    #region Constructor
    public PageTordViewModel()
    {
      Items = new ObservableCollection<TransportOrder>(Service.GetAllTords().Where(x => x.Status != TordStatus.Completed));
      LoadPopupItems();
    }
    #endregion

    #region Methods
    public void LoadPopupItems()
    {
      Locations = new ObservableCollection<Location>(TopologyService.Manager.GetAllLocations());

      TransportUnits = new ObservableCollection<TransportUnit>(TransportUnitManagement.Manager.GetAllTus());
    }

    public void SearchItems()
    {
      try
      {
        Items.Clear();

        var props = typeof(TransportOrder).GetProperties().Select(x => new KeyValuePair<string, PropertyInfo>($"{nameof(TransportOrder)}.{x.Name}".Translate(), x)).ToList();

        var prop = props.FirstOrDefault(x => x.Key == SearchType);

        var field = prop.Value.Name;
        var type = prop.Value.PropertyType.Namespace == typeof(TransportOrder).Namespace ? prop.Value.PropertyType : null;

        if (type != null)
          field = type.GetProperties().FirstOrDefault(e => e.IsDefined(typeof(IdentifierAttribute))).Name;

        foreach (var item in Service.GetAllTords(field, SearchValue, type).Where(x => x.Status != TordStatus.Completed))
        {
          Items.Add(item);
        }

      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
      
    }
    public void ClearFilters()
    {
      SearchValue = null;
      SearchType = null;
      Items.Clear();

      foreach (var item in Service.GetAllTords().Where(x => x.Status != TordStatus.Completed))
      {
        Items.Add(item);
      }
    }
    
    public async Task OnDeleteClick()
    {
      var win = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams
      {
        ContentTitle = "Popup.DeleteTordTitle".Translate(),
        ContentMessage = "Popup.DeleteTordMessage".Translate(),
        ButtonDefinitions = MessageBox.Avalonia.Enums.ButtonEnum.OkCancel,
        Icon = MessageBox.Avalonia.Enums.Icon.Warning,
        Style = MessageBox.Avalonia.Enums.Style.DarkMode,
        WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner,
        ShowInCenter = true
      });

      var r = await win.Show();

      if (r == MessageBox.Avalonia.Enums.ButtonResult.Ok)
      {
        Service.DeleteTord(SelectedItem.Id);
        ClearFilters();
      }
    }

    public void OnActivate()
    {
      try
      {
        Service.ChangeTordStatus(SelectedItem, SelectedItem.Tu.Location);
        ClearFilters();
      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
    }

    public void OnCreateClick()
    {
      LoadPopupItems();
      ErrorMessage = null;
      PopupOpen = true;
    }

    public void OnConfirm()
    {
      try
      {
        if (TargetTu == null)
        {
          ErrorMessage = "tord.creation.valid.fields".Translate();
          return;
        }

        ErrorMessage = null;

        Service.CreateTransportOrder(TargetTu, TargetLocation);

        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;
      }
    }

    public void ClosePopup()
    {
      PopupOpen = false;
    }
    #endregion
  }
}