﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using Services.Contracts.Extensions;
using WM.UserInterface.Models;

namespace WM.UserInterface.ViewModels
{
	public class MenuViewModel : ViewModelBase
	{
		#region Fields
		bool menuEnabled;
		#endregion

		#region Properties

		public bool MenuEnabled
		{
			get
			{
				return menuEnabled;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref menuEnabled, value);
				menuEnabled = value;
			}
		}

		#endregion

		#region Constructor
		public MenuViewModel()
		{
			MenuEnabled = true;
		}
		#endregion

		#region Methods
		
		public void OnMenuButtonClick()
		{
			MenuEnabled = !MenuEnabled;
		}

		#endregion

	}
}
