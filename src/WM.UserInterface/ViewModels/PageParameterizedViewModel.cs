﻿using Avalonia.Controls.Primitives;
using ReactiveUI;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;
using WM.ProcessingLayer.Management.SearchRules;

namespace WM.UserInterface.ViewModels
{
	public class PageParameterizedViewModel : ViewModelBase
	{
		#region Fields
		string _searchValue;
		string _searchType;
		ParameterizedDetail _selectedItem;
		bool createPopup;
		string errorMessage;
		bool popupOpen;
		#endregion

		#region Properties
		public IRulesManagement Service => RulesManagement.Manager;

		public ObservableCollection<ParameterizedDetail> Items { get; }

		public ObservableCollection<Rule> Rules { get; set; }

		public ObservableCollection<string> SourceEntities { get; }

		public ObservableCollection<string> TargetEntities { get; }

		public ObservableCollection<string> SourceProperties { get; set; }

		public ObservableCollection<string> TargetProperties { get; set; }

		public IList<ParameterizedOperator> OperatorList { get; } = Enum.GetValues(typeof(ParameterizedOperator)).Cast<ParameterizedOperator>().ToList();

		public string SearchValue
		{
			get
			{
				return _searchValue;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref _searchValue, value);
				_searchValue = value;
			}
		}

		public string SearchType
		{
			get
			{
				return _searchType;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref _searchType, value);
				_searchType = value;

				if (string.IsNullOrEmpty(value))
					SearchValue = string.Empty;
			}
		}

		public ParameterizedDetail SelectedItem
		{
			get
			{
				return _selectedItem;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref _selectedItem, value);
				_selectedItem = value;
			}
		}

		public bool CreatePopup
		{
			get
			{
				return createPopup;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref createPopup, value);
				createPopup = value;
			}
		}

		public string ErrorMessage
		{
			get
			{
				return errorMessage;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref errorMessage, value);
				errorMessage = value;
			}
		}

		public bool PopupOpen
		{
			get
			{
				return popupOpen;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref popupOpen, value);
				popupOpen = value;
			}
		}

		public IList<string> PropertySearchItems => typeof(ParameterizedDetail).GetProperties().Select(x => $"{nameof(ParameterizedDetail)}.{x.Name}".Translate()).ToList();
		#endregion

		#region Constructor
		public PageParameterizedViewModel()
		{
			Items = new ObservableCollection<ParameterizedDetail>(Service.GetAllParameterizedRules());
			SourceEntities = new ObservableCollection<string>(new[] { nameof(TransportUnit), nameof(Material) });
			TargetEntities = new ObservableCollection<string>(new[] { nameof(Location) });
			Rules = new ObservableCollection<Rule>(RulesManagement.Manager.GetAllRules());
		}
		#endregion

		#region Methods
		public void SearchItems()
		{
			try
			{
				Items.Clear();

				var props = typeof(ParameterizedDetail).GetProperties().Select(x => new KeyValuePair<string, PropertyInfo>($"{nameof(ParameterizedDetail)}.{x.Name}".Translate(), x)).ToList();

				foreach (var item in Service.GetAllParameterizedRules(props.FirstOrDefault(x => x.Key == SearchType).Value.Name, SearchValue))
				{
					Items.Add(item);
				}

			}
			catch (Exception e)
			{
				var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
				messageBoxStandardWindow.Show();
			}

		}

		public void ClearFilters()
		{
			SearchValue = null;
			SearchType = null;
			Items.Clear();

			foreach (var item in Service.GetAllParameterizedRules())
			{
				Items.Add(item);
			}
		}

		public void OnAddClick()
		{
			CreatePopup = true;
			SelectedItem = new ParameterizedDetail();
			ErrorMessage = null;
			PopupOpen = true;
		}

		public void OnEditClick()
		{
			CreatePopup = false;
			ErrorMessage = null;
			PopupOpen = true;
		}

		public async Task OnDeleteClick()
		{
			var win = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams
			{
				ContentTitle = "Popup.DeleteParameterizedRuleTitle".Translate(),
				ContentMessage = "Popup.DeleteParameterizedRuleMessage".Translate(),
				ButtonDefinitions = MessageBox.Avalonia.Enums.ButtonEnum.OkCancel,
				Icon = MessageBox.Avalonia.Enums.Icon.Warning,
				Style = MessageBox.Avalonia.Enums.Style.DarkMode,
				WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner,
				ShowInCenter = true
			});

			var r = await win.Show();

			if (r == MessageBox.Avalonia.Enums.ButtonResult.Ok)
			{
				Service.DeleteParameterizedRule(SelectedItem.Id);
				ClearFilters();
			}
		}

		public void OnConfirm()
		{
			if (CreatePopup)
				CreateNewItem();
			else
				EditItem();
		}

		public void EditItem()
		{
			try
			{
				ErrorMessage = null;

				Service.UpdateParameterizedRule(SelectedItem);

				PopupOpen = false;
				ClearFilters();
			}
			catch (Exception e)
			{
				ErrorMessage = e.Message;

			}
		}

		public void CreateNewItem()
		{
			try
			{
				ErrorMessage = null;

				Service.InsertParameterizedRule(SelectedItem);

				PopupOpen = false;
				ClearFilters();
			}
			catch (Exception e)
			{
				ErrorMessage = e.Message;
			}
		}

		public void ClosePopup()
		{
			PopupOpen = false;
		}


		public void SelectionSourceChanged()
		{
			if (SelectedItem.SourceEntity == nameof(Material))
				SourceProperties = new ObservableCollection<string>(typeof(Material).GetProperties().Select(x => x.Name).ToList());
			else if (SelectedItem.SourceEntity == nameof(TransportUnit))
				SourceProperties = new ObservableCollection<string>(typeof(TransportUnit).GetProperties().Select(x => x.Name).ToList());
		}

		public void SelectionTargetChanged()
		{
			if (SelectedItem.TargetEntity == nameof(Location))
				TargetProperties = new ObservableCollection<string>(typeof(Location).GetProperties().Select(x => x.Name).ToList());
		}
		#endregion
	}
}