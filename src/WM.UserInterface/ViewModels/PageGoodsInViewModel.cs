﻿using Avalonia.Controls.Primitives;
using ReactiveUI;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WM.Common.Entities;
using WM.ProcessingLayer.Contracts;
using WM.ProcessingLayer.Management;

namespace WM.UserInterface.ViewModels
{
	public class PageGoodsInViewModel : ViewModelBase
	{
		#region Fields
		string message;

		string materialType;

		string lotId;
		#endregion

		#region Properties
		public ITransportUnitManagement Service => TransportUnitManagement.Manager;

		public string Message
		{
			get
			{
				return message;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref message, value);
				message = value;
			}
		}

		public string MaterialType
		{
			get
			{
				return materialType;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref materialType, value);
				materialType = value;
			}
		}

		public string LotID
		{
			get
			{
				return lotId;
			}
			set
			{
				this.RaiseAndSetIfChanged(ref lotId, value);
				lotId = value;
			}
		}

		#endregion

		#region Constructor
		public PageGoodsInViewModel()
		{
			
		}
		#endregion

		#region Methods

		public void OnAddClick()
		{
			try
			{
				var tu = Service.ReceiveTu(LotID, MaterialType);
				Message = string.Format("goodsin.addlabel".Translate(), tu.Name);
			}
			catch (Exception e)
			{
				Message = e.Message;
			}
		}

		#endregion
	}
}