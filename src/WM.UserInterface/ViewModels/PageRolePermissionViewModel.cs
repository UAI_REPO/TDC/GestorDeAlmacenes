﻿using Avalonia.Controls.Primitives;
using ReactiveUI;
using Service.Security;
using Service.Security.Contracts;
using Service.Security.Entites.DB;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WM.Common.Entities;

namespace WM.UserInterface.ViewModels
{
	public class PageRolePermissionViewModel : ViewModelBase
	{
    #region Fields
    string _searchValue; 
    string _searchType;
    RolePermission _selectedItem;
    bool createPopup;
    string errorMessage;
    bool popupOpen;
    #endregion

    #region Properties
    public ISecurityDataService Service => SecurityDataService.Manager;

    public ObservableCollection<RolePermission> Items { get; }

    public string SearchValue
    {
      get
      {
        return _searchValue;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchValue, value);
        _searchValue = value;
      }
    }

    public string SearchType
    {
      get
      {
        return _searchType;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _searchType, value);
        _searchType = value;

        if (string.IsNullOrEmpty(value))
          SearchValue = string.Empty;
      }
    }

    public RolePermission SelectedItem
    {
      get
      {
        return _selectedItem;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref _selectedItem, value);
        _selectedItem = value;
      }
    }

    public bool CreatePopup
    {
      get
      {
        return createPopup;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref createPopup, value);
        createPopup = value;
      }
    }

    public string ErrorMessage
    {
      get
      {
        return errorMessage;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref errorMessage, value);
        errorMessage = value;
      }
    }

    public bool PopupOpen
    {
      get
      {
        return popupOpen;
      }
      set
      {
        this.RaiseAndSetIfChanged(ref popupOpen, value);
        popupOpen = value;
      }
    }

    public IList<string> PropertySearchItems => typeof(RolePermission).GetProperties().Select(x => $"{nameof(RolePermission)}.{x.Name}".Translate()).ToList();

    public ObservableCollection<Permission> Permissions { get; }

    public ObservableCollection<RoleDefinition> RoleDefinitions { get; }

    #endregion

    #region Constructor
    public PageRolePermissionViewModel()
    {
      Items = new ObservableCollection<RolePermission>(Service.GetAllRolePermission());

      Permissions = new ObservableCollection<Permission>(Service.GetAllPermission());
      RoleDefinitions = new ObservableCollection<RoleDefinition>(Service.GetAllRoleDefinitions());
    }
    #endregion

    #region Methods
    public void SearchItems()
    {
      try
      {
        Items.Clear();

        var props = typeof(RolePermission).GetProperties().Select(x => new KeyValuePair<string, PropertyInfo>($"{nameof(RolePermission)}.{x.Name}".Translate(), x)).ToList();

        var prop = props.FirstOrDefault(x => x.Key == SearchType);

        var field = prop.Value.Name;
        var type = prop.Value.PropertyType.Namespace == typeof(RolePermission).Namespace ? prop.Value.PropertyType : null;

        if (type != null)
          field = "Name";

        foreach (var item in Service.GetAllRolePermission(field, SearchValue, type))
        {
          Items.Add(item);
        }

      }
      catch (Exception e)
      {
        var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", e.Message);
        messageBoxStandardWindow.Show();
      }
      
    }
    
    public void ClearFilters()
    {
      SearchValue = null;
      SearchType = null;
      Items.Clear();

      foreach (var item in Service.GetAllRolePermission())
      {
        Items.Add(item);
      }
    }

    public void OnAddClick()
    {
      CreatePopup = true;
      SelectedItem = new RolePermission();
      ErrorMessage = null;
      PopupOpen = true;
    }

    public void OnEditClick()
    {
      CreatePopup = false;
      ErrorMessage = null;
      PopupOpen = true;
    }

    public async Task OnDeleteClick()
    {
      var win = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(new MessageBox.Avalonia.DTO.MessageBoxStandardParams
      {
        ContentTitle = "Popup.DeleteRolePermissionTitle".Translate(),
        ContentMessage = "Popup.DeleteRolePermissionMessage".Translate(),
        ButtonDefinitions = MessageBox.Avalonia.Enums.ButtonEnum.OkCancel,
        Icon = MessageBox.Avalonia.Enums.Icon.Warning,
        Style = MessageBox.Avalonia.Enums.Style.DarkMode,
        WindowStartupLocation = Avalonia.Controls.WindowStartupLocation.CenterOwner,
        ShowInCenter = true
      });

      var r = await win.Show();

      if (r == MessageBox.Avalonia.Enums.ButtonResult.Ok)
      {
        Service.DeleteRolePermission(SelectedItem.Id);
        ClearFilters();
      }
    }

    public void OnConfirm()
    {
      if (CreatePopup)
        CreateNewRoute();
      else
        EditRoute();
    }

    public void EditRoute()
    {
      try
      {
        ErrorMessage = null;

        Service.UpdateRolePermission(SelectedItem);

        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;

      }
    }

    public void CreateNewRoute()
    {
      try
      {
        ErrorMessage = null;

        Service.InsertRolePermission(SelectedItem);
        
        PopupOpen = false;
        ClearFilters();
      }
      catch (Exception e)
      {
        ErrorMessage = e.Message;
      }
    }

    public void ClosePopup()
    {
      PopupOpen = false;
    }
    
    #endregion
  }
}