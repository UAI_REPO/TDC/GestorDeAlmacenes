﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Logging.Serilog;
using Avalonia.ReactiveUI;
using WM.ProcessingLayer.Job;

namespace WM.UserInterface
{
	class Program
	{
		// The entry point. Things aren't ready yet, so at this point
		// you shouldn't use any Avalonia types or anything that expects
		// a SynchronizationContext to be ready
		public static void Main(string[] args)
		{
			var job = new AutomaticMovementsJob();
			Task.Run(() => job.Start());

			BuildAvaloniaApp().StartWithClassicDesktopLifetime(args);
		}

    // Avalonia configuration, don't remove; also used by visual designer.
    public static AppBuilder BuildAvaloniaApp()
				=> AppBuilder.Configure<App>()
						.UsePlatformDetect()
						.LogToDebug()
						.UseReactiveUI();
	}
}
