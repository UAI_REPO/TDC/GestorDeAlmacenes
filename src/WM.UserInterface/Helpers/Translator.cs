﻿using Avalonia.Data.Converters;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace WM.UserInterface.Helpers
{
	public class Translator : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return parameter.ToString().Translate();
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return value;
		}
	}
}
