﻿using Newtonsoft.Json;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Services.Entities.Logger;
using WM.UserInterface.Models;

namespace WM.UserInterface.Helpers
{
	public static class MenuLayout
	{
		public static IList<LayoutItem> GetLayout()
		{
			try
			{
				string jsonString = File.ReadAllText($"./Models/menu.json");
				return JsonConvert.DeserializeObject<IList<LayoutItem>>(jsonString);
			}
			catch (Exception e)
			{
				ServiceContract.WriteLog(LoggerLevel.Error, e, $"Error trying to load Localization file.");
				throw;
			}
		}
	}
}
