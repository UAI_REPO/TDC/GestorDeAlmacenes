﻿using Avalonia.Interactivity;
using System;
using System.Collections.Generic;
using System.Text;

namespace WM.UserInterface.Helpers
{
	public class MenuItemEventArgs: RoutedEventArgs
	{
		public string UI { get; set; }
	}
}
