﻿using System;

namespace WM.Common.Exceptions
{
	public class DaoException : Exception
	{
		private readonly string InnerMessage;

		public DaoException(string message)
		{
			InnerMessage = message;
		}

		public override string Message => $"Error en la conexion con la base. {InnerMessage}";
	}
}