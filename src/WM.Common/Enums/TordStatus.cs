﻿namespace WM.Common.Entities
{
	public enum TordStatus
	{
		Created,
		Ready,
		Faulty,
		Suspended,
		Completed
	}
}