﻿namespace WM.Common.Entities
{
	public enum TordMovementStatus
	{
		Active,
		Paused,
		Pickup,
		Deposit,
		Completed
	}
}