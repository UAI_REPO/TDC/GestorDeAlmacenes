﻿namespace WM.Common.Entities
{
	public enum LocationType
	{
		Warehouse,
		Bay,
		Area,
		Bin,
		Position,
		Special,
		TransportMedium
	}
}