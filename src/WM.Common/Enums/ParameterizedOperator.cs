﻿namespace WM.Common.Entities
{
	public enum ParameterizedOperator
	{
		Equal,
		NotEqual,
		Greater,
		GreaterThan,
		Less,
		LessThan
	}
}