﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WM.Common.Entities
{
	public class Route
	{
		[Identifier]
		public long Id { get; set; }

		public Location Source { get; set; }

		public Location Target { get; set; }

		public Location Next { get; set; }

		public bool Enabled { get; set; }

	}
}
