﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WM.Common.Entities
{
	public class ParameterizedDetail
	{
		[Identifier]
		public int Id { get; set; }

		public Rule Rule { get; set; }

		public string SourceEntity { get; set; }

		public string SourceProperty { get; set; }

		public string SourceValue { get; set; }

		public ParameterizedOperator SourceOperator { get; set; }

		public string TargetEntity { get; set; }

		public string TargetProperty { get; set; }

		public string TargetValue { get; set; }

		public ParameterizedOperator TargetOperator { get; set; }

	}
}
