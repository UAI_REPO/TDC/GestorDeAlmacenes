﻿namespace WM.Common.Entities
{
	public class Location
	{
    [Identifier]
    public string Name { get; set; }
    public LocationType Type { get; set; }
    public string Class { get; set; }
    public string Parent { get; set; }

    public int Capacity { get; set; }
    public int W { get; set; }
    public int B { get; set; }
    public int A { get; set; }
    public int X { get; set; }
    public int Y { get; set; }
    public int Z { get; set; }

  }
}