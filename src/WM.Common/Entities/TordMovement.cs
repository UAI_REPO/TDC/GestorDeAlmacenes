﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WM.Common.Entities
{
	public class TordMovement
  {
		[Identifier]
		public long Id { get; set; }

		public TransportUnit Tu { get; set; }

		public TordMovementStatus Status { get; set; }

		public TransportOrder Tord { get; set; }

    public Location TargetLocation { get; set; }
 
  }
}
