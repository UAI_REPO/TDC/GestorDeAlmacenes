﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WM.Common.Entities
{
	public class TransportUnit
	{
		[Identifier]
		public string Name { get; set; }

		public string LotId { get; set; }

		public bool IsObserved { get; set; }

		public Location Location { get; set; }

		public Material Material { get; set; }
	}
}
