﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WM.Common.Entities
{
	public class Material
	{
		[Identifier]
		public string Name { get; set; }

		public string Description { get; set; }

	}
}
