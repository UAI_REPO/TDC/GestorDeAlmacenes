﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WM.Common.Entities
{
	public class Rule
	{
		[Identifier]
		public long Id { get; set; }

		public string Name { get; set; }

		public Material Type { get; set; }

		public LocationType Level { get; set; }

		public int Priority { get; set; }

		public bool Enabled { get; set; }

	}
}
