﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WM.Common.Entities
{
	public class Transportist
	{
		[Identifier]
		public int UserId { get; set; }

		public string Type { get; set; }

	}
}
