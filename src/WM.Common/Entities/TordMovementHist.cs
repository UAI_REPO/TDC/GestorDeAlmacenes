﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WM.Common.Entities
{
	public class TordMovementHist : TordMovement
	{
		public long TordMovementId { get; set; }

		public DateTime UpdatedAt { get; set; }
	}
}
