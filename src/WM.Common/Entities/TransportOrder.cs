﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WM.Common.Entities
{
	public class TransportOrder
	{
    [Identifier]
    public long Id { get; set; }

		public TransportUnit Tu { get; set; }

		public TordStatus Status { get; set; }

    public string Message { get; set; }

    public Location LastLocation { get; set; }

    public Location ActualLocation { get; set; }

    public Location TargetLocation { get; set; }

    public string CreatedBy { get; set; }

    public DateTime? CreatedAt { get; set; }

    public string UpdatedBy { get; set; }

    public DateTime? UpdatedAt { get; set; }
 
  }
}
