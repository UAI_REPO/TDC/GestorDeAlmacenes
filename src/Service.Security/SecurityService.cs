﻿using Service.Security.Contracts;
using Service.Security.Entites;
using Service.Security.Entites.DB;
using Service.Security.Exceptions;
using Services.Contracts;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WM.DataAccess;

namespace Service.Security
{
	public class SecurityService : ISecurityService
	{
		#region Singleton
		private static object lockObj = new object();

		private static ISecurityService service;
		public static ISecurityService Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new SecurityService();

					return service;
				}
			}
		}
		#endregion

		#region Fields
		User currentUser;

		IList<RoleBase> roles;

		#endregion

		#region Interface Implementations
		public IList<string> GetUIPermissions()
		{
			try
			{
				IList<string> result = new List<string>();

				GetPermissionForRole(roles, result);

				return result.Distinct().ToList();

			}
			catch (Exception e)
			{
				($"Error getting permissions").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
			
		}

		public void GetPermissionForRole(IList<RoleBase> roles, IList<string> result)
		{
			foreach (var item in roles)
			{
				if (item is RoleAsign)
					GetPermissionForRole(((RoleAsign)item).GetChildren(), result);
				else
					result.Add(item.Name);
			}
		}

		public void SetCurrentUser(User user)
		{
			try
			{
				var roleauth = new List<RoleBase>();

				currentUser = user;
				if (currentUser == null)
					return;

				//Get user permissions
				var userPermissions = DataAccessRegister.GetDataAccess<UserPermission>()
					.GetAllElements(nameof(UserPermission.User.Id), user.Id, typeof(User));

				foreach (var permission in userPermissions)
				{
					roleauth.Add(new RoleAuthorization() { Name = permission.Permission.Name });
				}

				//Get all user roles
				var userRoles = DataAccessRegister.GetDataAccess<UserRole>().GetAllElements(nameof(UserRole.User.Id), user.Id, typeof(User));

				//Get all roles and match with dependencies
				var roles = DataAccessRegister.GetDataAccess<Role>().GetAllElements();

				//Get all permissiones for role
				var permissions = DataAccessRegister.GetDataAccess<RolePermission>().GetAllElements();

				foreach (var userRole in userRoles)
				{
					roleauth.Add(PopulateRoleAuth(userRole.RoleDefinition.Id, roles, permissions));
				}

				this.roles = roleauth;
			}
			catch (Exception e)
			{
				($"Error getting permissions").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}

		}

		public User GetUser(string name, string pass)
		{
			try
			{
				($"Get user name [{name}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<User>();

				var user = da.GetAllElements(nameof(User.Name), name).FirstOrDefault();

				if (user == null)
					throw new UserNotFoundException(name);

				if (user.Password != pass)
					throw new WorngPasswordException();

				user.Password = "*********";
				return user;
			}
			catch (Exception e)
			{
				($"Error getting user").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public string GetCurrentUser()
		{
			return currentUser.Name;
		}

		public long CalculateDVH(User user)
		{
			long scretkey = 2539404912046012734;
			long result = 87631289362193; //initial value
			result += user.Id.GetHashCode() % scretkey;
			result += user.Name.GetHashCode() % scretkey;
			result += user.Password.GetHashCode() % scretkey;

			return result;
		}

		public long CalculateDVV()
		{
			long scretkey = 1298734120947106123;
			long result = 93824723896491238; //initial value

			var users = SecurityDataService.Manager.GetAllUsers();
			
			foreach (var user in users)
			{
				result += user.DVH % scretkey;
			}

			return result;
		}

		public bool CheckDV()
		{
			return CalculateDVV() == SecurityDataService.Manager.GetDVV();
		}

		public void SetDVV()
		{
			var da = DataAccessRegister.GetDataAccess<DVV>();
			var dvv = da.GetAllElements().FirstOrDefault();
			dvv.Digit = CalculateDVV();
			da.UpdateElement(dvv);
		}
		#endregion

		#region Helpers

		private RoleBase PopulateRoleAuth(long idRole, IList<Role> roles, IList<RolePermission> permissions)
		{
			var result = new RoleAsign() { Name = idRole.ToString() };

			foreach (var p in permissions.Where(x => x.RoleDefinition.Id == idRole))
			{
				result.Add(new RoleAuthorization() { Name = p.Permission.Name });
			}
			var currentroles = roles.Where(x => x.RoleDefinitionId == idRole);
			foreach (var currentrole in currentroles)
			{
				if (currentrole.InheritanceRoleId == null)
				{
					foreach (var p in permissions.Where(x => x.RoleDefinition.Id == currentrole.RoleDefinitionId))
					{
						result.Add(new RoleAuthorization() { Name = p.Permission.Name });
					}
				}
				else
				{
					result.Add(PopulateRoleAuth(currentrole.InheritanceRoleId.GetValueOrDefault(), roles, permissions));
				}
			}

			return result;
		}

		#endregion
	}
}
