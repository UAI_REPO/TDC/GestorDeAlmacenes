﻿using Service.Security.Entites.DB;
using System.Collections.Generic;

namespace Service.Security.Contracts
{
	public interface ISecurityService
	{
		IList<string> GetUIPermissions();

		User GetUser(string name, string pass);

		void SetCurrentUser(User user);

		string GetCurrentUser();

		long CalculateDVH(User user);

		long CalculateDVV();

		bool CheckDV();

		void SetDVV();
	}
}