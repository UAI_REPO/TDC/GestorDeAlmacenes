﻿
using Service.Security.Entites.DB;
using System;
using System.Collections.Generic;

namespace Service.Security.Contracts
{
	public interface ISecurityDataService
	{
		public IList<Permission> GetAllPermission(string prop = null, object value = null, Type entity = null);

		public void InsertPermission(Permission permission);

		public void UpdatePermission(Permission permission);

		public void DeletePermission(long id);

		public IList<Role> GetAllRoles(string prop = null, object value = null, Type entity = null);
		public void InsertRole(Role item);

		public void UpdateRole(Role role);

		public void DeleteRole(long id);


		IList<RoleDefinition> GetAllRoleDefinitions(string prop = null, object value = null, Type entity = null);
		void InsertRoleDefinition(RoleDefinition item);
		void UpdateRoleDefinition(RoleDefinition role);
		void DeleteRoleDefinition(int id);

		IList<RolePermission> GetAllRolePermission(string prop = null, object value = null, Type entity = null);
		void InsertRolePermission(RolePermission item);
		void UpdateRolePermission(RolePermission role);
		void DeleteRolePermission(int id);


		IList<User> GetAllUsers(string prop = null, object value = null, Type entity = null);
		void InsertUser(User item);
		void UpdateUser(User user);
		void DeleteUser(int id);

		void DeleteUserPermission(int id);
		void UpdateUserPermission(UserPermission permission);
		void InsertUserPermission(UserPermission item);
		IList<UserPermission> GetAllUserPermission(string prop = null, object value = null, Type entity = null);

		IList<UserRole> GetAllUserRoles(string prop = null, object value = null, Type entity = null);
		void InsertUserRole(UserRole item);
		void UpdateUserRole(UserRole role);
		void DeleteUserRole(int id);
		long GetDVV();
	}
}