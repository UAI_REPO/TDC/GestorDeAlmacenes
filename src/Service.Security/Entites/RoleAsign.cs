﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service.Security.Entites
{
	public class RoleAsign : RoleBase
	{
		private List<RoleBase> children;

		public RoleAsign()
		{
			children = new List<RoleBase>();
		}

		public override void Add(RoleBase role)
		{
			children.Add(role);
		}

		public override IList<RoleBase> GetChildren()
		{
			return children;
		}

		public override void Remove(RoleBase role)
		{
			children.RemoveAll(x => x.Name == role.Name);
		}
	}
}
