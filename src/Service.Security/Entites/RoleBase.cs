﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Security.Entites
{
	public abstract class RoleBase
	{
		public string Name { get; set; }

		public abstract void Add(RoleBase role);

		public abstract void Remove(RoleBase role);

		public abstract IList<RoleBase> GetChildren();
	}
}
