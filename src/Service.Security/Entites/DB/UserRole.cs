﻿using System;
using System.Collections.Generic;
using System.Text;
using WM.Common.Entities;

namespace Service.Security.Entites.DB
{
	public class UserRole
	{
		[Identifier]
		public int Id { get; set; }

		public User User { get; set; }

		public RoleDefinition RoleDefinition { get; set; }

	}
}
