﻿using System;
using System.Collections.Generic;
using System.Text;
using WM.Common.Entities;

namespace Service.Security.Entites.DB
{
	public class RolePermission
	{
		[Identifier]
		public int Id { get; set; }

		public RoleDefinition RoleDefinition { get; set; }

		public Permission Permission { get; set; }

	}
}
