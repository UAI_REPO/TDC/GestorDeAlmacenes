﻿using System;
using System.Collections.Generic;
using System.Text;
using WM.Common.Entities;

namespace Service.Security.Entites.DB
{
	public class UserPermission
	{
		[Identifier]
		public int Id { get; set; }

		public User User { get; set; }

		public Permission Permission { get; set; }

	}
}
