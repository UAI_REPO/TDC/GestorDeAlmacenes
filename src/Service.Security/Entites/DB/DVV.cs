﻿using System;
using System.Collections.Generic;
using System.Text;
using WM.Common.Entities;

namespace Service.Security.Entites.DB
{
	public class DVV
	{
		[Identifier]
		public int Id { get; set; }
		public long Digit { get; set; }
	}
}
