﻿using System;
using System.Collections.Generic;
using System.Text;
using WM.Common.Entities;

namespace Service.Security.Entites.DB
{
	public class Permission
	{
		[Identifier]
		public int Id { get; set; }

		public string Name { get; set; }


	}
}
