﻿using Services.Contracts.Extensions;
using System;
using System.Runtime.Serialization;

namespace Service.Security.Exceptions
{
	public class WorngPasswordException : Exception
	{
		public WorngPasswordException() : base($"WorngPasswordException".Translate())
		{
		}

	}
}