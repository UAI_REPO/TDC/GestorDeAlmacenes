﻿using Services.Contracts.Extensions;
using System;

namespace Service.Security.Exceptions
{
	public class UserNotFoundException : Exception
	{
		public UserNotFoundException()
		{
		}

		public UserNotFoundException(string user) : base(string.Format($"UserNotFoundException".Translate(), user))
		{

		}

	}
}