﻿using Service.Security.Contracts;
using Service.Security.Entites;
using Service.Security.Entites.DB;
using Service.Security.Exceptions;
using Services.Contracts;
using Services.Contracts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WM.DataAccess;

namespace Service.Security
{
	public class SecurityDataService : ISecurityDataService
	{
		#region Singleton
		private static object lockObj = new object();

		private static ISecurityDataService service;
		public static ISecurityDataService Manager
		{
			get
			{
				lock (lockObj)
				{
					if (service == null)
						service = new SecurityDataService();

					return service;
				}
			}
		}
		#endregion

		#region Interface Implementations

		public IList<Permission> GetAllPermission(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all permissions. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Permission>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all Permissions. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void InsertPermission(Permission permission)
		{
			try
			{
				($"Insert new Permission name [{permission.Name}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Permission>();

				da.InsertElement(permission);
			}
			catch (Exception e)
			{
				($"Error inserting new permission.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdatePermission(Permission permission)
		{
			try
			{
				($"update permission id [{permission.Id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Permission>();

				da.UpdateElement(permission);
			}
			catch (Exception e)
			{
				($"Error updating permission.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeletePermission(long id)
		{
			try
			{
				($"Delete permission Name [{id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Permission>();
				da.DeleteElement(id);
			}
			catch (Exception e)
			{
				($"Error deleting material Name [{id}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public IList<Role> GetAllRoles(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all roles. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Role>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all roles. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void InsertRole(Role item)
		{
			try
			{
				($"Insert new Role [{item.RoleDefinitionId}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Role>();

				da.InsertElement(item);
			}
			catch (Exception e)
			{
				($"Error inserting new role.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdateRole(Role role)
		{
			try
			{
				($"update permission id [{role.Id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Role>();

				da.UpdateElement(role);
			}
			catch (Exception e)
			{
				($"Error updating role.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteRole(long id)
		{
			try
			{
				($"Delete role Name [{id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<Role>();
				da.DeleteElement(id);
			}
			catch (Exception e)
			{
				($"Error deleting role Id [{id}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public IList<RoleDefinition> GetAllRoleDefinitions(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all role definitions. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<RoleDefinition>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all role definitions.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void InsertRoleDefinition(RoleDefinition item)
		{
			try
			{
				($"Insert new Role definition [{item.Id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<RoleDefinition>();

				da.InsertElement(item);
			}
			catch (Exception e)
			{
				($"Error inserting new role definition.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdateRoleDefinition(RoleDefinition role)
		{
			try
			{
				($"update role defintion id [{role.Id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<RoleDefinition>();

				da.UpdateElement(role);
			}
			catch (Exception e)
			{
				($"Error updating role defintion.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteRoleDefinition(int id)
		{
			try
			{
				($"Delete role definition Name [{id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<RoleDefinition>();
				da.DeleteElement(id);
			}
			catch (Exception e)
			{
				($"Error deleting role definition Id [{id}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public IList<RolePermission> GetAllRolePermission(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all role permissions. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<RolePermission>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all role permissions.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void InsertRolePermission(RolePermission item)
		{
			try
			{
				($"Insert new Role permission [{item.Id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<RolePermission>();

				da.InsertElement(item);
			}
			catch (Exception e)
			{
				($"Error inserting new role permission.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdateRolePermission(RolePermission role)
		{
			try
			{
				($"update role permission id [{role.Id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<RolePermission>();

				da.UpdateElement(role);
			}
			catch (Exception e)
			{
				($"Error updating role permission.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteRolePermission(int id)
		{
			try
			{
				($"Delete role permission Name [{id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<RolePermission>();
				da.DeleteElement(id);
			}
			catch (Exception e)
			{
				($"Error deleting role permission Id [{id}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public IList<User> GetAllUsers(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all users. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<User>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all users.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void InsertUser(User item)
		{
			try
			{
				($"Insert new user [{item.Name}]").LogInfo();

				item.DVH = SecurityService.Manager.CalculateDVH(item);

				var da = DataAccessRegister.GetDataAccess<User>();

				da.InsertElement(item);

				SecurityService.Manager.SetDVV();
			}
			catch (Exception e)
			{
				($"Error inserting new user.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdateUser(User user)
		{
			try
			{
				($"update user id [{user.Id}]").LogInfo();

				user.DVH = SecurityService.Manager.CalculateDVH(user);

				var da = DataAccessRegister.GetDataAccess<User>();

				da.UpdateElement(user);

				SecurityService.Manager.SetDVV();
			}
			catch (Exception e)
			{
				($"Error updating role permission.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteUser(int id)
		{
			try
			{
				($"Delete user Id [{id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<User>();
				da.DeleteElement(id);

				SecurityService.Manager.SetDVV();
			}
			catch (Exception e)
			{
				($"Error deleting user Id [{id}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public IList<UserPermission> GetAllUserPermission(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all user permissions. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<UserPermission>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all user permissions.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void InsertUserPermission(UserPermission item)
		{
			try
			{
				($"Insert new User permission [{item.Id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<UserPermission>();

				da.InsertElement(item);
			}
			catch (Exception e)
			{
				($"Error inserting new user permission.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdateUserPermission(UserPermission permission)
		{
			try
			{
				($"update user permission id [{permission.Id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<UserPermission>();

				da.UpdateElement(permission);
			}
			catch (Exception e)
			{
				($"Error updating user permission.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteUserPermission(int id)
		{
			try
			{
				($"Delete user permission Name [{id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<UserPermission>();
				da.DeleteElement(id);
			}
			catch (Exception e)
			{
				($"Error deleting user permission Id [{id}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public IList<UserRole> GetAllUserRoles(string prop = null, object value = null, Type entity = null)
		{
			try
			{
				($"Getting all user roles. Filter [{prop} : {value}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<UserRole>();
				if (prop != null && value != null)
					return da.GetAllElements(prop, value, entity);

				return da.GetAllElements();
			}
			catch (Exception e)
			{
				($"Error getting all user roles.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void InsertUserRole(UserRole item)
		{
			try
			{
				($"Insert new User role [{item.Id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<UserRole>();

				da.InsertElement(item);
			}
			catch (Exception e)
			{
				($"Error inserting new user role.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void UpdateUserRole(UserRole role)
		{
			try
			{
				($"update user role id [{role.Id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<UserRole>();

				da.UpdateElement(role);
			}
			catch (Exception e)
			{
				($"Error updating user role.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public void DeleteUserRole(int id)
		{
			try
			{
				($"Delete user role Name [{id}]").LogInfo();

				var da = DataAccessRegister.GetDataAccess<UserRole>();
				da.DeleteElement(id);
			}
			catch (Exception e)
			{
				($"Error deleting user role Id [{id}]. ").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		public long GetDVV()
		{
			try
			{
				($"Getting DVV").LogInfo();

				var da = DataAccessRegister.GetDataAccess<DVV>();
				var dvv = da.GetAllElements().FirstOrDefault();

				return dvv.Digit;
			}
			catch (Exception e)
			{
				($"Error getting DVV.").LogError(e);

				ServiceContract.ManageException(e);
				throw;
			}
		}

		#endregion

		#region Helpers

		#endregion
	}
}
