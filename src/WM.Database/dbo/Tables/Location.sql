﻿CREATE TABLE [dbo].[Location] (
    [Name]     VARCHAR (50) NOT NULL,
    [Type]     INT          NOT NULL,
    [Capacity] INT          NOT NULL,
    [Class]    VARCHAR (50) NULL,
    [Parent]   VARCHAR (50) NULL,
    [W]        INT          NOT NULL,
    [B]        INT          NOT NULL,
    [A]        INT          NOT NULL,
    [X]        INT          NOT NULL,
    [Y]        INT          NOT NULL,
    [Z]        INT          NOT NULL,
    CONSTRAINT [PK__Location__737584F720405262] PRIMARY KEY CLUSTERED ([Name] ASC),
    CONSTRAINT [FK_Location_ToLocation] FOREIGN KEY ([Parent]) REFERENCES [dbo].[Location] ([Name])
);



GO

CREATE INDEX [IX_Location_Name] ON [dbo].[Location] ([Name])
