﻿CREATE TABLE [dbo].[Role] (
    [Id]                INT IDENTITY (1, 1) NOT NULL,
    [RoleDefinitionId]  INT NOT NULL,
    [InheritanceRoleId] INT NULL,
    CONSTRAINT [PK_Role1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Role_RoleDefinition] FOREIGN KEY ([RoleDefinitionId]) REFERENCES [dbo].[RoleDefinition] ([Id]),
    CONSTRAINT [FK_Role_RoleDefinition1] FOREIGN KEY ([InheritanceRoleId]) REFERENCES [dbo].[RoleDefinition] ([Id])
);


