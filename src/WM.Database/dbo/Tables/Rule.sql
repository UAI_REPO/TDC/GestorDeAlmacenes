﻿CREATE TABLE [dbo].[Rule] (
    [Id]       BIGINT       IDENTITY (1, 1) NOT NULL,
    [Name]     VARCHAR (50) NOT NULL,
    [TypeName] VARCHAR (50) NOT NULL,
    [Level]    INT          NOT NULL,
    [Priority] INT          NOT NULL,
    [Enabled]  BIT          NOT NULL,
    CONSTRAINT [PK_Rule] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Rule_Material] FOREIGN KEY ([TypeName]) REFERENCES [dbo].[Material] ([Name])
);

