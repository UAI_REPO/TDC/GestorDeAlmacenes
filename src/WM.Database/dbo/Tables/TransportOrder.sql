﻿CREATE TABLE [dbo].[TransportOrder] (
    [Id]                 INT          IDENTITY (1, 1) NOT NULL,
    [TuName]             VARCHAR (50) NOT NULL,
    [Status]             INT          NOT NULL,
    [LastLocationName]   VARCHAR (50) NULL,
    [ActualLocationName] VARCHAR (50) NULL,
    [TargetLocationName] VARCHAR (50) NULL,
    [CreatedBy]          VARCHAR (50) NULL,
    [CreatedAt]          DATETIME     NULL,
    [UpdatedAt]          DATETIME     NULL,
    [UpdatedBy]          VARCHAR (50) NULL,
    [Message]            VARCHAR (50) NULL,
    CONSTRAINT [PK__Transpor__3214EC0799296C82] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TransportOrder_ToActualLocation] FOREIGN KEY ([ActualLocationName]) REFERENCES [dbo].[Location] ([Name]),
    CONSTRAINT [FK_TransportOrder_ToLastLocation] FOREIGN KEY ([LastLocationName]) REFERENCES [dbo].[Location] ([Name]),
    CONSTRAINT [FK_TransportOrder_ToTargetLocation] FOREIGN KEY ([TargetLocationName]) REFERENCES [dbo].[Location] ([Name]),
    CONSTRAINT [FK_TransportOrder_ToTransportUnit] FOREIGN KEY ([TuName]) REFERENCES [dbo].[TransportUnit] ([Name])
);


GO
ALTER TABLE [dbo].[TransportOrder] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON);







GO

CREATE INDEX [IX_TransportOrder_Id] ON [dbo].[TransportOrder] ([Id])

GO

CREATE TRIGGER [dbo].PopulateTransportOrderHist 
   ON  [dbo].TransportOrder 
   AFTER INSERT,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [dbo].TransportOrderHist(
        TransportOrderId, 
        TuName,
        Status,
        LastLocationName,
        ActualLocationName,
        TargetLocationName, 
        CreatedBy, 
        CreatedAt,
		UpdatedBy, 
        UpdatedAt,
		Message
    )
    SELECT
        i.Id,
        i.TuName,
        i.Status,
        i.LastLocationName,
		i.ActualLocationName,
        i.TargetLocationName,
        i.CreatedBy,
		i.CreatedAt,
		i.UpdatedBy,
		i.UpdatedAt,
		i.Message
    FROM
        inserted i
END