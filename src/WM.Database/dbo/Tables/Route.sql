﻿CREATE TABLE [dbo].[Route] (
    [Id]         BIGINT       IDENTITY (1, 1) NOT NULL,
    [SourceName] VARCHAR (50) NOT NULL,
    [TargetName] VARCHAR (50) NOT NULL,
    [NextName]   VARCHAR (50) NOT NULL,
    [Enabled]    BIT          NOT NULL
);

