﻿CREATE TABLE [dbo].[RoleDefinition] (
    [Id]   INT          IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (50) NULL,
    CONSTRAINT [PK__RoleDefi__3214EC0756A5DFA8] PRIMARY KEY CLUSTERED ([Id] ASC)
);

