﻿CREATE TABLE [dbo].[RolePermission] (
    [Id]               INT IDENTITY (1, 1) NOT NULL,
    [RoleDefinitionId] INT NULL,
    [PermissionId]     INT NULL,
    CONSTRAINT [PK__Role__3214EC07903EBDB6] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RolePermission_Permission] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[Permission] ([Id]),
    CONSTRAINT [FK_RolePermission_RoleDefinition] FOREIGN KEY ([RoleDefinitionId]) REFERENCES [dbo].[RoleDefinition] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IdIndex]
    ON [dbo].[RolePermission]([Id] ASC);

