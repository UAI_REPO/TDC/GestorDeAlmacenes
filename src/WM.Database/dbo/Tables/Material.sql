﻿CREATE TABLE [dbo].[Material]
(
    [Name] VARCHAR(50) NOT NULL, 
    [Description] VARCHAR(200) NULL, 
    PRIMARY KEY ([Name])
)

GO

CREATE INDEX [IX_Material_Id] ON [dbo].[Material] ([Name])
