﻿CREATE TABLE [dbo].[TransportOrderHist] (
    [Id]                 INT          IDENTITY (1, 1) NOT NULL,
    [TransportOrderId]   INT          NOT NULL,
    [TuName]             VARCHAR (50) NULL,
    [Status]             INT          NULL,
    [LastLocationName]   VARCHAR (50) NULL,
    [ActualLocationName] VARCHAR (50) NULL,
    [TargetLocationName] VARCHAR (50) NULL,
    [CreatedBy]          VARCHAR (50) NULL,
    [CreatedAt]          DATETIME     NULL,
    [UpdatedAt]          DATETIME     NULL,
    [UpdatedBy]          VARCHAR (50) NULL,
    [Message]            VARCHAR (50) NULL,
    CONSTRAINT [PK__Transpor__3214EC0799296C83] PRIMARY KEY CLUSTERED ([Id] ASC)
);

