﻿CREATE TABLE [dbo].[TransportUnit] (
    [Name]         VARCHAR (50) NOT NULL,
    [LotId]        VARCHAR (50) NOT NULL,
    [LocationName] VARCHAR (50) NOT NULL,
    [MaterialName] VARCHAR (50) NOT NULL,
    [IsObserved]   BIT          NOT NULL,
    CONSTRAINT [PK__Transpor__737584F796D940C4] PRIMARY KEY CLUSTERED ([Name] ASC),
    CONSTRAINT [FK_TransportUnit_ToLocation] FOREIGN KEY ([LocationName]) REFERENCES [dbo].[Location] ([Name]),
    CONSTRAINT [FK_TransportUnit_ToMaterial] FOREIGN KEY ([MaterialName]) REFERENCES [dbo].[Material] ([Name])
);



GO

CREATE INDEX [IX_TransportUnit_Name] ON [dbo].[TransportUnit] ([Name])
