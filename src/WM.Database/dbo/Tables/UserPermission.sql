﻿CREATE TABLE [dbo].[UserPermission] (
    [Id]           INT IDENTITY (1, 1) NOT NULL,
    [UserId]       INT NOT NULL,
    [PermissionId] INT NOT NULL,
    CONSTRAINT [PK_UserPermission] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserPermission_Permission] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[Permission] ([Id]),
    CONSTRAINT [FK_UserPermission_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);

