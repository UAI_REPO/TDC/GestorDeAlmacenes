﻿CREATE TABLE [dbo].[TordMovementHist] (
    [Id]                 INT          IDENTITY (1, 1) NOT NULL,
    [TordMovementId]     INT          NOT NULL,
    [Status]             INT          NULL,
    [TordId]             INT          NULL,
    [TargetLocationName] VARCHAR (50) NULL,
    [TuName]             VARCHAR (50) NULL,
    [UpdatedAt]          DATETIME     NULL,
    CONSTRAINT [PK__TordMoveHist__3214EC07B5071BCB] PRIMARY KEY CLUSTERED ([Id] ASC)
);

