﻿CREATE TABLE [dbo].[TordMovement] (
    [Id]                 INT          IDENTITY (1, 1) NOT NULL,
    [Status]             INT          NULL,
    [TordId]             INT          NULL,
    [TargetLocationName] VARCHAR (50) NULL,
    [TuName]             VARCHAR (50) NULL,
    CONSTRAINT [PK__TordMove__3214EC07B5071BCB] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TordMovement_TransportOrder] FOREIGN KEY ([TordId]) REFERENCES [dbo].[TransportOrder] ([Id]),
    CONSTRAINT [FK_TordMovement_TransportUnit] FOREIGN KEY ([TuName]) REFERENCES [dbo].[TransportUnit] ([Name])
);


GO
ALTER TABLE [dbo].[TordMovement] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = ON);





GO

CREATE TRIGGER [dbo].[PopulateTordMovementHist] 
   ON  [dbo].[TordMovement] 
   AFTER INSERT,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [dbo].TordMovementHist(
        TordMovementId,
        Status,
        TordId,
        TargetLocationName, 
        TuName,
		UpdatedAt
    )
    SELECT
        i.Id,
		i.Status,
		i.TordId,
		i.TargetLocationName,
        i.TuName,
		GETDATE()
    FROM
        inserted i
END